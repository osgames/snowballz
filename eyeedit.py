# Eye expression editor for snowball surprise
# Based on the Soya 3D tutorial 'raypicking-2'
# Copyright (C) 2004 Matthew Marshall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



import sys, os, os.path, soya, soya.cube, soya.sphere, soya.sdlconst

#from soya.opengl import *

import cartooneye

import bezeir

soya.init()
soya.path.append(os.path.join(os.path.dirname(sys.argv[0]), "data"))

# Creates the scene.

scene = soya.World()


# DragDropWorld is a world that allows to dragdrop its content with the mouse.

class DragDropWorld(soya.World):
  def __init__(self, parent):
    soya.World.__init__(self, parent)
    
    # The object we are currently dragdroping (None => no dragdrop).
    
    self.dragdroping = None
    
    # The impact point
    
    self.impact = None
    
    
  def begin_round(self):
    soya.World.begin_round(self)
    
    # Processes the events
    
    for event in soya.process_event():
      
      # Mouse down initiates the dragdrop.
      
      if   event[0] == soya.sdlconst.MOUSEBUTTONDOWN:
        
        # The event give us the 2D mouse coordinates in pixel. The camera.coord2d_to_3d
        # convert these 2D pixel coordinates into a soy.Point object.
        
        mouse = camera.coord2d_to_3d(event[2], event[3])
        
        # Performs a raypicking, starting at the camera and going toward the mouse.
        # The vector_to method returns the vector between 2 positions.
        # This raypicking grabs anything that is under the mouse. Raypicking returns
        # None if nothing is encountered, or a (impact, normal) tuple, where impact is the
        # position of the impact and normal is the normal vector at this position.
        # The object encountered is impact.parent ; here, we don't need the normal.
        
        result = self.raypick(camera, camera.vector_to(mouse))
        if result:
          self.impact, normal = result
          self.dragdroping = self.impact.parent
          
          # Converts impact into the camera coordinate system, in order to get its Z value.
          # camera.coord2d_to_3d cannot choose a Z value for you, so you need to pass it
          # as a third argument (it defaults to -1.0). Then, we computes the old mouse
          # position, which has the same Z value than impact.
          
          self.impact.convert_to(camera)
          self.old_mouse = camera.coord2d_to_3d(event[2], event[3], self.impact.z)
          
          
      # Mouse up ends the dragdrop.
      
      elif event[0] == soya.sdlconst.MOUSEBUTTONUP:
        self.dragdroping = None
        
        
      # Mouse motion moves the dragdroping object, if there is one.
      
      elif event[0] == soya.sdlconst.MOUSEMOTION:
        if self.dragdroping:
          
          # Computes the new mouse position, at the same Z value than impact.
          
          new_mouse = camera.coord2d_to_3d(event[1], event[2], self.impact.z)
          
          # Translates dragdroping by a vector starting at old_mouse and ending at
          # new_mouse.
          
          self.dragdroping.add_vector(self.old_mouse.vector_to(new_mouse))
          
          # Store the current mouse position.
          
          self.old_mouse = new_mouse
          
          global Eye
          Eye.sync_segments()
          
        #Have the eyes look at the mouse.
        mousepoint = camera.coord2d_to_3d(event[1], event[2])
        #print mousepoint
        #global eyes
        #for eye in eyes:
        #  mousepoint.convert_to(eye)
        #  eye.lookDirection = eye.vector_to(mousepoint)
        #  eye.lookDirection.normalize()
          #print eye.lookDirection
          

class ControlPoint(soya.Volume):
  def __init__(self,parent,shape,syncpoint=None):
    soya.Volume.__init__(self,parent,shape)
    self.syncpoint = syncpoint
    self.vector_with = [] #Add the same vector to these...
        
  def sync(self):
    if not self.syncpoint:return
    self.syncpoint.x = self.x
    self.syncpoint.y = self.y
    self.syncpoint.z = self.z
    
  def add_vector(self,vector):
    soya.Volume.add_vector(self,vector)
    self.sync()
    for p in self.vector_with:
      p.add_vector(vector)

  def __pow__(self,n):
    return soya.Vector(self.parent,self.x**n,self.y**n,self.z**n)
  def __mul__(self,n):
    return soya.Vector(self.parent,self.x*n,self.y*n,self.z*n)

class BezeirPoint(soya.Point):
  def __add__(self,other):
    return BezeirPoint(self.parent,self.x+other.x,self.y+other.y,self.z+other.z)
  def __mul__(self,num):
    return BezeirPoint(self.parent,self.x*num,self.y*num,self.z*num)
  def __pow__(self,num):
    return BezeirPoint(self.parent,self.x**num,self.y**num,self.z**num)
  
class HorizontalOnlyControlPoint(ControlPoint):
  def add_vector(self,vector):
    vector.y=0.0; vector.z=0.0
    ControlPoint.add_vector(self,vector)

class BaseControlPoint(ControlPoint):
  def __init__(self,parent,shape,syncpoint = None):
    ControlPoint.__init__(self,parent,shape,syncpoint)
    self.arms=[None,None]
  def add_vector(self,vector):
    ControlPoint.add_vector(self,vector)
    for a in self.arms:
      if a:
        a.add_vector(vector,False)
        
class ArmControlPoint(ControlPoint):
  def __init__(self,parent,shape,syncpoint=None):
    ControlPoint.__init__(self,parent,shape,syncpoint)
    self.base = None
    self.other_arm = None
  def add_vector(self,vector,move_other=True):
    ControlPoint.add_vector(self,vector)
    if move_other and self.other_arm and self.base:
      l = self.base.vector_to(self.other_arm).length()
      self.other_arm.add_vector(self.other_arm.vector_to(self.base),False)
      v = self.vector_to(self.base)
      v.set_length(l)
      self.other_arm.add_vector(v,False)
      
class VisualBeizer(soya.PythonCoordSyst):
  def __init__(self,parent,material,bezeir):
    soya.PythonCoordSyst.__init__(self,parent)
    self.bezeir = bezeir
    self.material = material
  def batch(self):
    return 1,self,self.material
  def render(self):
    try:
        from soya.opengl import glDisable,glBegin,glVertex3f,glEnd,glEnable,\
                                GL_LIGHTING,GL_DEPTH_TEST,GL_LINE_STRIP
        self.material.activate()
        glDisable(GL_LIGHTING)
        glDisable(GL_DEPTH_TEST)
        #glHint(GL_LINE_SMOOTH_HINT, GL_NICEST)
        glBegin(GL_LINE_STRIP)
        for i in range(101):
            v = self.bezeir.point(i/100.0)
            glVertex3f(v.x,v.y,v.z)
          
        glEnd()
        glEnable(GL_LIGHTING)
        glEnable(GL_DEPTH_TEST)
    except:
        import traceback,sys
        traceback.print_exc()
        sys.exit(1)
        
class EyelidControl(soya.Volume):
  def __init__(self,parent,shape,eye,isTop,x,miny,maxy,z,minangle,maxangle):
    soya.Volume.__init__(self,parent,shape)
    self.eye = eye
    self.isTop = isTop
    self.lockx = x
    self.miny=miny
    self.maxy=maxy
    self.lockz=z
    self.minangle=minangle
    self.maxangle=maxangle
    
    self.set_pos()
    
  def set_pos(self):
    if self.isTop:
      a = self.eye.targetTopEyelidAngle
    else:
      a = self.eye.targetBottomEyelidAngle
    self.set_xyz(self.lockx,(a-self.minangle)/(self.maxangle-self.minangle)*(self.maxy-self.miny)+self.miny,self.lockz)
  
  def add_vector(self,vector):
    soya.Volume.add_vector(self,vector)
    
    #Make sure we have not gone too far up or down.
    self.y = max(min(self.y,self.maxy),self.miny)
    
    a = (self.y-self.miny)/(self.maxy-self.miny)*(self.maxangle-self.minangle)+self.minangle
    if self.isTop:
      self.eye.targetTopEyelidAngle = a
    else:
      self.eye.targetBottomEyelidAngle = a
    
    self.set_pos() #Re-aline, in case we have been draged away.


# Creates a dragdrop world.

world = DragDropWorld(scene)

#eyes = (cartooneye.Eye(world),cartooneye.Eye(world))
#eyes[0].set_xyz(.5,0.0,-.5)
#eyes[1].set_xyz(-.5,0.0,-.5)

# Adds some volumes with different shapes, at different positions.

base_material   = soya.Material(); base_material.diffuse = (1.0, 0.0, 0.0, 1.0)
arm_material   = soya.Material(); arm_material.diffuse = (0.0, 1.0, 1.0, 1.0)
line_material = soya.Material(); line_material.diffuse = (0.0,1.0,0.0,1.0)
#lid_material = soya.Material(); lid_material.diffuse = (1.0,0.0,0.0,1.0)
#ball_material = soya.Material(); ball_material.texture = soya.Image.get("eyeball.png")

#EyelidControl(world,soya.sphere.Sphere(None, control_material,5,5).shapify(),eyes[1],True ,-1,   .1,1.5, -.5,  90.0, 180.0).scale(.1,.1,.1)
#EyelidControl(world,soya.sphere.Sphere(None, control_material,5,5).shapify(),eyes[1],False,-1, -1.5,-.5, -.5, 180.0, 270.0).scale(.1,.1,.1)

#EyelidControl(world,soya.sphere.Sphere(None, control_material,5,5).shapify(),eyes[0],True , 1,   .1,1.5, -.5,  90.0, 180.0).scale(.1,.1,.1)
#EyelidControl(world,soya.sphere.Sphere(None, control_material,5,5).shapify(),eyes[0],False, 1, -1.5,-.5, -.5, 180.0, 270.0).scale(.1,.1,.1)

#for eye in eyes:
#  eye.setres(16,5)
#  eye.lid_material = lid_material
#  eye.ball_material = ball_material
#  controlworld = soya.World(world)
#  controlworld.add_vector(controlworld.vector_to(eye))
#  controlworld.add_vector(soya.Vector(None,-.5,-.5,.5))
#  controlworld.scale(1.0/(eye.lattice.res[0]-1),1.0/(eye.lattice.res[1]-1),1.0)
#  for x in range(eye.lattice.res[0]):
#    for y in range(eye.lattice.res[1]):
#      ControlPoint(controlworld, soya.sphere.Sphere(None, control_material,5,5).shapify(), eye, (x,y)).scale(.1,.1,.1)

#soya.Volume(world, soya.cube.Cube(None, green).shapify()).set_xyz( 0.0, -1.0, 0.0)
#soya.Volume(world, soya.cube.Cube(None, blue ).shapify()).set_xyz( 1.0, -1.0, -1.0)

#soya.Volume(world, soya.sphere.Sphere().shapify()).set_xyz(1.0, 1.0, 0.0)


base_shape = soya.sphere.Sphere(None,base_material)
base_shape.scale(0.03,0.03,0.03)
base_shape = base_shape.shapify()
arm_shape = soya.sphere.Sphere(None,arm_material)
arm_shape.scale(0.03,0.03,0.03)
arm_shape = arm_shape.shapify()


class EyeControl1(soya.World): #This defines the eye shape when looking from the front.
  def __init__(self,parent,syncpoints):
    soya.World.__init__(self,parent)
    controls = [None]*12
    
    for i in range(0,12,3):
      controls[i] = BaseControlPoint(self,base_shape,syncpoints[i])
      controls[i+1] = ArmControlPoint(self,arm_shape,syncpoints[i+1])
      controls[i].arms[0] = controls[i+1]
      controls[i+1].base = controls[i]
      
    controls[11] = ArmControlPoint(self,arm_shape,syncpoints[11])
    controls[0].arms[1] = controls[11]
    controls[11].base = controls[0]
    controls[11].other_arm = controls[1]
    controls[1].other_arm = controls[11] 

    for i in range(2,9,3):
      controls[i] = ArmControlPoint(self,arm_shape,syncpoints[i])
      controls[i].base = controls[i+1]
      controls[i+1].arms[1] = controls[i]
      controls[i].other_arm = controls[i+2]
      controls[i+2].other_arm = controls[i]
      
    pos = [
    (.5,0), (.5,.3),
    (.3,.5), (0,.5), (-.3,.5),
    (-.5,.3), (-.5,0), (-.5,-.3),
    (-.3,-.5), (0,-.5), (.3,-.5),
    (.5,-.3)]
    for i in range(12):
      controls[i].x,controls[i].y = pos[i]

    for c in controls:
      c.sync()
    
    curve = bezeir.circle(controls)
    VisualBeizer(self,line_material,curve)

class EyeControl2(soya.World): #This is for controlling the eye shape from the side.
  def __init__(self,parent,syncpoints):
    soya.World.__init__(self,parent)
    controls = [None]*5
    
    for i in [0,2,4]:
      controls[i] = HorizontalOnlyControlPoint(self,base_shape,syncpoints[i])
    for i in [1,3]:
      controls[i] = ControlPoint(self,None,syncpoints[i])
    
    controls[0].vector_with = [controls[1]]
    controls[4].vector_with = [controls[3]]
    
    self.rotate_lateral(90)
    
    for c in controls[:2]:
      c.z = .2
    for c in controls[3:]:
      c.z = -.2
    for c in controls[1:4]:
      c.y = .5
    
    for c in controls:
      c.sync()
    
    curve = bezeir.curvelink(controls,2)
    VisualBeizer(self,line_material,curve)
    
Eye = cartooneye.Eye(world)
Eye.bezeircircle = bezeir.circle([BezeirPoint(Eye) for i in [None]*12])
Eye.crossbezeir = bezeir.curvelink([BezeirPoint(Eye) for i in [None]*5],2)
#Eye.lid_material = line_material

EyeControl1(world,Eye.bezeircircle.points).set_xyz(1,-0.5,0)
EyeControl2(world,Eye.crossbezeir.points).set_xyz(1,.5,0)

Eye.set_res()


#Eye.calculate()

# Adds a light.

light = soya.Light(scene)
light.set_xyz(2.0, 1.7, 2.0)

# Creates a camera.

camera = soya.Camera(scene)
camera.set_xyz(0.0, 0.0, 2.5)
scene.ortho = True

soya.set_root_widget(camera)

#soya.toggle_wireframe()

# Main loop

#soya.render()
soya.Idler(scene).idle()


# TODO / exercice : turn this demo into a puzzle game !
