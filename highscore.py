"""Highscore module for saving and accessing highscores.

The current idea is to save the highest 20 at any given time. Names don't
matter, i.e. you can have entires like "John Doe - 250; John Doe - 400", etc.
The scores are stored in ~/.snowballz, in an XML format, handled by
ElementTree.

The current set-up is this: there is a root element whose children are game
elements. The children of any game element are player elements, and each such
element has a score and name attributes. Note that currently we don't care about
the names - there could be 5 players called "John", and still each would be
considered a unique player and have his own player element.
"""

__revision__ = "$Id$" # For CVS checkouts, and for high scores with pylint :-D

import os, os.path
from elementtree.ElementTree import ElementTree, Element, SubElement


MAX_SCORES = 10 # How many scores we save
SCORE_FILENAME = os.path.expanduser("~/.snowballz/highscores.xml")
SCORE_TREE = None # The score tree is initialized at the end of this module
_root = None # The root of the score tree, defined for convinience and initialized at the end as well


def add_score(game, player_name, score):
    """Adds a score to a player in a game.
    
    If the score doesn't qualify as a highscore, the entry is ignored.
    If there is a collision and the score is equal to the lowest score,
    then it replaces the old score."""
    
    add_score = True # By default, we will add this highscore, but this may change in the code
    score = str(score) # ElementTree can't save ints :-/    
    game_element = _find_game(game)
    if game_element:
        # The entry for this game exists
        children = game_element.getchildren()
        if len(children) >= MAX_SCORES:
            # Highscore collision may occur!
            # Find thet lowest-scoring player first
            lowest_scoring_player = children[0]
            lowest_score = lowest_scoring_player.get("score")
            for player in children[1:]:
                if player.get("score") < lowest_score:
                    lowest_scoring_player = player
                    lowest_score = player.get("score")
            
            if score >= lowest_score:
                # Add the new highscore, remove the old one
                game_element.remove(lowest_scoring_player)
            else:
                # We won't add this score
                add_score = False
    else:
        # Create the game entry
        game_element = SubElement(parent=_root, tag="Game",
                                  attrib={"name": game})
        
    if add_score:
        SubElement(parent=game_element, tag="Player",
                   attrib={"name": player_name, "score": score})
                       
    # Write changes
    SCORE_TREE.write(SCORE_FILENAME)
        
def get_scores(game):
    """Returns an ordered list of (player, score) tuples for a given game.
    
    If no such game exists, raise NoSuchGameError."""
    
    game_element = _find_game(game)
    if not game_element:
        raise NoSuchGameError("Game %s has no highscores!" % game)
        
    scores = []
    for player in game_element.getchildren():
        scores.append((player.get("name"), int(player.get("score"))))
        
    # We sort the scores but we want the highest scores in the beginning,
    # and thus we multiply the score comparison by -1 (reverse it).
    scores.sort(lambda score1, score2: -1*cmp(score1[1], score2[1]))
        
    return scores
    

# Internal stuff - initialization, exceptions, etc.

def _init_score_tree():
    """Initialize the highscore tree.
    
    If the relevant directory/file don't exist, create them."""
    
    # First, make sure the directory even exists, and create it if it doesn't.
    if not os.path.exists(os.path.expanduser("~/.snowballz")):
        os.mkdir(os.path.expanduser("~/.snowballz"))
    
    if not os.path.exists(SCORE_FILENAME):
        # File doesn't exist, create it and the tree.
        score_tree = ElementTree(element=Element(tag="HighscoreTree"))
        score_tree.write(SCORE_FILENAME) # Save the tree
    else:
        # File exists, read the highscores from it.
        score_tree = ElementTree(file=SCORE_FILENAME)
        
    return score_tree
    

SCORE_TREE = _init_score_tree()
_root = SCORE_TREE.getroot()


def _find_game(game_name):
    """Find and return the game element that matches the game_name."""
    
    for game_element in _root.getchildren():
        if game_element.get("name") == game_name:
            return game_element
    
    return None
    

class HighscoreException(Exception):
    """Dummy parent exception."""
    pass
    
    
class NoSuchGameError(HighscoreException):
    """Called when the user tries to get scores of a non-existant game."""
    pass
