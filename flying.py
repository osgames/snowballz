#! /usr/bin/python -O

# standard modules

import sys, os, os.path 

import traceback

import random

import math 
from math import sqrt,atan,degrees

# test for any non-standard modules
# try to give some nice instructions if things dont work 

try:
  import soya
  import soya.widget as widget
  import soya.sdlconst as sdlconst
  import soya.particle
  from soya.opengl import *
  import soya.cube
except ImportError:
  # if we do this then the user can see the -actual- error 
  traceback.print_exc()

  print 
  print "[ERR] There was an error loading Soya. This could mean that you do not have it installed."  
  print "[ERR] Download it from http://home.gna.org/oomadness/en/soya/index.html"
  sys.exit(1)

try:
  from elementtree import ElementTree
except ImportError:
  traceback.print_exc()
  
  print 
  print "[ERR] There was an error loading elementtree. This could mean that you do not have it installed."
  print "[ERR] Download it from http://effbot.org/downloads/#elementtree" 
  sys.exit(1)

HERE = os.path.dirname(sys.argv[0])
soya.path.append(os.path.join(HERE, "data"))

# we need to import ourself so that levels using flying.Bonus etc can be handled
import flying

# snowballz imports
import font
import menu
import player
import idlers
import particles

idlers.HERE=HERE

# globals 
scene=None
snow=None
level=None
wind=None
character = None


class Level(soya.World):
  def __init__(self,parent=None,shape=None):
    soya.World.__init__(self,parent,shape)

    self.levelname="UN-Named"

class Action:
  def __init__(self, action):
    self.action = action

# The available actions
ACTION_WAIT          = 0
ACTION_ADVANCE       = 1
ACTION_ADVANCE_LEFT  = 2
ACTION_ADVANCE_RIGHT = 3
ACTION_TURN_LEFT     = 4
ACTION_TURN_RIGHT    = 5
ACTION_GO_BACK       = 6
ACTION_GO_BACK_LEFT  = 7
ACTION_GO_BACK_RIGHT = 8
ACTION_JUMP          = 9

#ANIM_STILL="still"
ANIM_STILL="Stand_Still"
ANIM_SLIDE="Stand_Still"
ANIM_FLAP="Flap"
ANIM_JUMP="Jump"
ANIM_FALLING="Falling"

class KeyboardControler:
  """A controler is an object that gives orders to a character.
Here, we define a keyboard based controler, but there may be mouse-based or IA-based
controlers.
Notice that the unique method is called "next", which allows to use Python generator
as controller."""
  def __init__(self):
    self.left_key_down = self.right_key_down = self.up_key_down = self.down_key_down = 0
    self.action_table = {
      (0, 0, 1, 0) : ACTION_ADVANCE,
      (1, 0, 1, 0) : ACTION_ADVANCE_LEFT,
      (0, 1, 1, 0) : ACTION_ADVANCE_RIGHT,
      (1, 0, 0, 0) : ACTION_TURN_LEFT,
      (0, 1, 0, 0) : ACTION_TURN_RIGHT,
      (0, 0, 0, 1) : ACTION_GO_BACK,
      (1, 0, 0, 1) : ACTION_GO_BACK_LEFT,
      (0, 1, 0, 1) : ACTION_GO_BACK_RIGHT}
    
  def next(self):
    """Returns the next action"""
    jump = 0
    
    for event in soya.process_event():
      if event[0]==sdlconst.VIDEORESIZE:
        move_widgets()
      elif event[0]==sdlconst.QUIT:
        sys.exit()
      if event[0] == sdlconst.KEYDOWN:
        if   (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          soya.IDLER.stop() # Quit the game
        
        elif event[1] == sdlconst.K_LSHIFT:
          # Shift key is for flying
          # Contrary to other action, jump is only performed once, at the beginning of
          # the jump.
          jump = 1
          
        elif event[1] == sdlconst.K_LEFT:  self.left_key_down  = 1
        elif event[1] == sdlconst.K_RIGHT: self.right_key_down = 1
        elif event[1] == sdlconst.K_UP:    self.up_key_down    = 1
        elif event[1] == sdlconst.K_DOWN:  self.down_key_down  = 1
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
          
        
      elif event[0] == sdlconst.KEYUP:
        if   event[1] == sdlconst.K_LEFT:  self.left_key_down  = 0
        elif event[1] == sdlconst.K_RIGHT: self.right_key_down = 0
        elif event[1] == sdlconst.K_UP:    self.up_key_down    = 0
        elif event[1] == sdlconst.K_DOWN:  self.down_key_down  = 0
    
    if jump: return Action(ACTION_JUMP)
    
    return Action(self.action_table.get((self.left_key_down, self.right_key_down,
                                         self.up_key_down, self.down_key_down),
                                        ACTION_WAIT))

class Character(player.Player):
  """A character in the game."""
  def __init__(self, parent, controler):
    player.Player.__init__(self, parent,controler)

    self.particles=particles.PlayerParticles(parent)
   
    self.flying = 0
    self.on_ground=0

    self.power=100.
    self.score=0
    self.bonus=0
    self.bonuscount=0
    self.target_points=0

  def do_it(self,angle,prop):
    air_density=0.000637
    speed=self.speed.z
   
    chord=4.
    span=8.
    planform=chord*span

    #drag
    cdf0=[0.01,0.0074,0.004,0.009,0.013,0.023,0.05,0.12,0.21]
    a=[-60.,-40.,0.,20.,40.,60.,80.,100.,120.]

    cd=0.5

    for i in range(0,8):
      if a[i]<=angle and a[i+1]>angle:
        cd=cdf0[i]-(a[i]-angle)*(cdf0[i]-cdf0[i+1]) / (a[i]-a[i+1])

    #lift
    clf0=[-.54, -.2, 0.2, .57, 0.92, 1.21, 1.43, 1.4, 1.0]

    cl=0.

    for i in range(0,8):
      if a[i]<=angle and a[i+1]>angle:
        cl=clf0[i]-(a[i]-angle)*(clf0[i]-clf0[i+1]) / (a[i]-a[i+1])


    drag=cd*0.5*air_density*(speed*speed)*planform
    lift=cl*0.5*air_density*(speed*speed)*planform

    print angle,"%.2f" % lift,drag,self.speed.z

    self.speed.y+=lift*prop
    self.speed.z+=drag*prop
    
    return lift,drag


  def got_bonus(self,obj):
    print "got bonus",obj,id(obj)
    self.score+=obj.points
    self.bonus+=obj.points
    self.bonuscount+=1
    obj.visible=0
    self.power+=obj.power
    soya.IDLER.score_label.text="pts: %d" %self.score

    p=particles.BonusParticles(self.parent,self.speed)
    p.move(obj)

  def got_target(self,obj):
    print "got target"
    self.speed.x=0.
    self.speed.y=0.
    self.speed.z=0.
    self.target_points=obj.points
    soya.IDLER.score('hit target!!',smallmsg=obj.name)

  def got_out(self):
    print "got out"
    self.speed.x=0.
    self.speed.y=0.
    self.speed.z=0.
    soya.IDLER.score('OUT!!',bad=True)
  
  def begin_round(self):
    if soya.IDLER.state==idlers.PLAYING:   
      self.begin_action(self.controler.next())

    soya.World.begin_round(self)
  
  def begin_action(self, action):
    if soya.IDLER.state!=idlers.PLAYING: return 

    # Reset
    self.rotation_speed = 0.0
    
    # If the haracter is flying, we don't want to reset speed.y to 0.0 !!!
    #if (not self.flying) and self.speed.y > 0.0: self.speed.y = 0.0
    
    animation = ANIM_STILL

    # Determine the character rotation
    if   action.action in (ACTION_TURN_LEFT, ACTION_ADVANCE_LEFT, ACTION_GO_BACK_LEFT):
      self.rotation_speed = 4.0
      animation = ANIM_SLIDE
    elif action.action in (ACTION_TURN_RIGHT, ACTION_ADVANCE_RIGHT, ACTION_GO_BACK_RIGHT):
      self.rotation_speed = -4.0
      animation = ANIM_SLIDE

    if   action.action in (ACTION_ADVANCE, ACTION_ADVANCE_LEFT, ACTION_ADVANCE_RIGHT):
      self.speed.z += -0.01
      animation = ANIM_SLIDE
    elif action.action in (ACTION_GO_BACK, ACTION_GO_BACK_LEFT, ACTION_GO_BACK_RIGHT):
      self.speed.z += 0.06
      animation = ANIM_SLIDE

    """
    # Determine the character speed
    if self.on_ground:
      if   action.action in (ACTION_ADVANCE, ACTION_ADVANCE_LEFT, ACTION_ADVANCE_RIGHT):
        self.speed.z += -0.01
        animation = ANIM_SLIDE
      elif action.action in (ACTION_GO_BACK, ACTION_GO_BACK_LEFT, ACTION_GO_BACK_RIGHT):
        self.speed.z += 0.06
        animation = ANIM_SLIDE
    else:
      if   action.action in (ACTION_ADVANCE, ACTION_ADVANCE_LEFT, ACTION_ADVANCE_RIGHT):
        self.wry+=2.
      elif action.action in (ACTION_GO_BACK, ACTION_GO_BACK_LEFT, ACTION_GO_BACK_RIGHT):
        self.wry-=2.
    """
  
    new_center = self.center + self.speed
    bigest=max(self.radius,self.radius_y+.1,self.speed.length())
    bigest=max(self.radius,self.radius_y+.1)
    #print self.radius,self.radius_y+.1,self.speed.length(),bigest
    context = scene.RaypickContext(new_center, bigest)
    
    # Gets the ground, and check if the character is falling
    #print "d",self.down
    #print self.down
    r = context.raypick(new_center, self.down, bigest, 3)
    #nc=new_center.copy()
    #nd=nc.copy()
    #nd.y-=4.
    #print nc,self.down

    #self.cube.move(new_center)
    #self.cube.y-=0.1+self.radius_y

    if action.action == ACTION_JUMP and self.power>0.1 and self.on_ground==0:
      self.flying = 1
      self.speed.y += 0.5
      #animation=ANIM_FLAP
      self.perso.animate_execute_action(ANIM_FLAP)
    else:
      self.flying=0

    """
    try:
      print r
      print r[1].parent.visible
    except:
      pass
    """

    if r and r[1].parent.visible==1:
      cobj=r[1].parent

      if isinstance(cobj,flying.FrictionVolume):
        self.on_ground=1
      
        ground, ground_normal = r
        ground.convert_to(self)
        ground_normal.convert_to(self)

        try:
          yr=atan(ground.y/ground.z)
          yr=degrees(yr)
          self.wyr=yr
        except:
          pass
        
        ground_normal.y*=cobj.ydamp*0.01
        self.speed.add_mul_vector(cobj.friction,ground_normal)
        self.speed.y = ground.y
      
        if self.speed.z>1.0:
          self.speed.z-=.1

      if type(cobj)==flying.Target:
        self.got_target(cobj)
      elif type(cobj) ==flying.Bonus:
        print "top cokksuin"
        self.got_bonus(cobj)
      elif type(cobj)==soya.Land:
        self.got_out()
       
      # flying is only possible if we are on ground
      
    else:
      self.on_ground=0
      # No ground => start falling
      # Test the fall with the pit behind the second house
      #if self.flying==0: animation = ANIM_STILL
      
      # If the vertical speed is negative, the jump is over
      #if self.speed.y < 0.0: self.flying = 0
      animation=ANIM_FALLING
      
    new_center = self.center + self.speed
    
    # The movement (defined by the speed vector) may be impossible if the character
    # would encounter a wall.
    
    for vec in (self.left, self.right, self.front, self.back, self.up):
      r = context.raypick(new_center, vec, bigest)
      if r and r[0].parent.visible==1:
        # The ray encounters a wall => the character cannot perform the planned movement.
        # We compute a correction vector, and add it to the speed vector, as well as to
        # new_center (for the following raypicks ; remember that
        # new_center = self.center + self.speed, so if speed has changed, we must update
        # it).
        
        collision, wall_normal = r
        if type(collision.parent) == flying.Bonus:
          print "side collision",vec
          self.got_bonus(collision.parent)
         
          continue

        hypo = vec.length() * self.radius - (new_center >> collision).length()
        correction = wall_normal * hypo
        
        # Theorical formula, but more complex and identical result
        #angle = (180.0 - vec.angle_to(wall_normal)) / 180.0 * math.pi
        #correction = wall_normal * hypo * math.cos(angle)
        
        self.speed += correction
        new_center += correction

    """
    if self.wrx!=self.rx:
      self.perso.rotate_vertical(-(self.wrx-self.rx))
      self.rx=self.wrx
    """

    if self.wry!=self.ry:
      self.perso.rotate_vertical(-(self.wry-self.ry))
      self.ry=self.wry

    #print self.speed.y,self.y
    self.play_animation(animation)

      
  def advance_time(self, proportion):
    soya.World.advance_time(self, proportion)

    if self.flying and self.power>0.1:
      self.power-=5.*proportion

    # completely arbitary speed display
    soya.IDLER.speed_label.text="spd: %.2f" % (self.speed.length()*100)
    soya.IDLER.power_label.text="pow: %.2f" % (self.power)
    soya.IDLER.altitude_label.text="alt: %.2f" % (self.y)

    snow.move(self)
    self.particles.move(self)

    if soya.IDLER.state!=idlers.PLAYING: return 

    if self.on_ground==0:
      self.add_mul_vector(proportion,wind)
      self.speed.y-=.03*proportion


      """
      lift=drag=0.

      if self.ry<-5:
        lift=.2
        drag=.02
      elif self.ry>-5 and self.ry<5:
        lift=-.1
        drag=0.
      elif self.ry >5:
        lift=-.3
        drag=-.02

      print self.ry,lift,drag
        
      self.speed.y+=lift*proportion*-self.speed.z
      self.speed.z+=drag*proportion
      """
      #lift,drag=self.do_it(self.rx,proportion)
    
    #self.speed.y+=lift*proportion
    #self.speed.z-=drag*proportion
    
    #if self.speed.y<-1.: self.speed.y=-1.

    if self.y<-100. and soya.IDLER.state==1:
      self.got_out()

    self.add_mul_vector(proportion, self.speed)

    self.rotate_lateral(proportion * self.rotation_speed)

class Idler(idlers.SnowballzIdler):
  def start_waiting(self):
    global character

    self.game_rounds+=1

    # Loads the level, and put it in the scene
    self.state=1
    
    try: 
      scene.remove(level)
    except TypeError:
      # Can't remove level, as level is not set properly yet
      pass

    print "Idler.start_level creating level and wind..."
    
    try:
      self.level = soya.World.get(self.levels[self.game_rounds])
      scene.add(self.level)
    except ValueError:
      # Oops! No such level!
      # TODO: the game can't just crash like this...
      print >> sys.stderr, "[ERR]: level '"+self.levels[self.game_rounds]+\
                           "' doesn't exist!"
      sys.exit()

    wind=soya.Vector(scene,random.random()-.5,random.random()-.5,random.random()-.5)/2.

    try:
      self.level.remove(character)
      self.level.remove(character.particles)
    except (TypeError, ValueError):
      # Can't remove anything because it's not set yet, or has already been removed
      pass
    
    # Creates a character in the level, with a keyboard controler
    character = Character(self.level, KeyboardControler())
    character.set_xyz(0.,80. ,-75.)
    character.rotate_lateral(180.)
    character.power=100.
  
    snow.move(character)

    # adds our fixed camera
    self.camera = soya.Camera(scene)
    self.camera.z=scene.get_box()[1].z+400
    self.camera.y=100.
    self.camera.back = 470.0
    self.camera.look_at(character)

    # for the waiting loop to move the camera around
    self.camera_move=-1.

    soya.set_root_widget(widget.Group())
    soya.root_widget.add(widget.FPSLabel())
    soya.root_widget.add(self.camera)

##    color=(.4,.4,.9,.7)

    self.make_text_widgets()
    self.make_overlay()
    self.make_title()
    
    self.move_widgets()

  def start_playing(self):
    print "start playin"
    soya.root_widget.remove(self.camera)

    pos=self.camera.position()

    self.camera = soya.TravelingCamera(scene)
    traveling=soya.FixTraveling(soya.Point(character,0.,6.,6),character,smooth_move=1)
    self.camera.add_traveling(traveling)
    self.camera.move(pos)
    self.camera.back=470.

    soya.set_root_widget(widget.Group())
    soya.root_widget.add(widget.FPSLabel())
    soya.root_widget.add(self.camera)

    color=(.4,.4,.9,.7)

    self.make_text_widgets()
    self.make_overlay()
    self.move_widgets()

  def score(self,msg,bad=False,smallmsg=''):
    msgcol=(0.9,0.3,0.2,0.9)

    soya.root_widget.remove(self.score_label)
    soya.root_widget.remove(self.power_label)
    soya.root_widget.remove(self.altitude_label)
    soya.root_widget.remove(self.speed_label)
    soya.root_widget.remove(self.overlay)
    
    # Computer scores to display and to save
    character.bonuscount+=1
    powerbonus=int(character.power) * character.bonuscount *5
    character.score+=powerbonus
    targetbonus=character.target_points*character.bonuscount
    character.score+=targetbonus
    self.total_score+=character.score
    
    # Messages to print
    message_text = [msg,
                    "bonus: %d" % character.bonus,
                    "power bonus: %d x %d x 5 = %d" % (int(character.power),
                                                       character.bonuscount,
                                                       powerbonus),
                    "target: %d x %d = %d" % (character.target_points,
                                              character.bonuscount,
                                              targetbonus),
                    "score: %d" % character.score,
                    smallmsg,
                    "total: %d" % self.total_score]
                    
    tops = range(140, 501, 60) # 6 top values, indicates where the message will be printed
    
    for message, top in zip(message_text, tops):
      # Create new labels
      message=soya.widget.Label(soya.root_widget, text=message,
                                font=font.normal, color=msgcol)
      message.width=self.camera.width
      message.top=top
      message.align=1

    try:
      character.play_animation(ANIM_STILL)
      
      smoke=soya.particle.FlagFirework(level)
      smoke.auto_generate_particle=True
    
      if not bad:
        smoke.set_colors((0.1, 0.1, 0.2, 0.4), (0.3, 0.3, 0.5, 0.4), (0.3, 0.3, 0.5, 0.4), (0.1, 0.1, 0.1, 0.4))
      else:
        smoke.set_colors((0.9, 0.1, 0.1, 0.4), (0.6, 0.3, 0.3, 0.4), (0.5, 0.3, 0.3, 0.4), (0.1, 0.1, 0.1, 0.4))
    
      smoke.scale(4.,2.,4.)
      self.fireworks=smoke
    
      self.fireworks.move(character)
    
      traveling = soya.ThirdPersonTraveling(character,smooth_move=1)
      #traveling=soya.FixTraveling(soya.Point(character,0.,6.,6),character,smooth_move=1)
      
      traveling.distance = 8.0
    
      self.camera.add_traveling(traveling)
      self.camera.back = 470.0

    except: # TODO: catch explicit exceptions only
      import traceback
      traceback.print_exc()
      sys.exit()

    self.cmove=0.
    character.on_ground=1

    self.set_state(idlers.FINISHED)

  def advance_time_waiting(self,proportion):
    self.advance_time_default(proportion)
    self.camera.z+=self.camera_move*proportion
   
    b1,b2=scene.get_box()

    if self.camera.z>b2.z+500 or self.camera.z<b1.z+200:
      self.camera_move=-self.camera_move
  
    snow.move(self.camera)

    self.camera.look_at(character)
  
  def advance_time_finished(self,proportion):
    character.turn_lateral(9.*proportion)

    if self.cmove<50:
      character.y+=proportion*.4
      self.cmove+=proportion*.4

    self.fireworks.move(character)
  
    self.advance_time_default(proportion)  

  def begin_round_waiting(self):
    self.begin_round_default()

    for event in soya.process_event():
      print "brw",event
      if event[0] == sdlconst.KEYDOWN:
        print "kdown"
        if (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          self.stop() # Quit the game
        
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
        elif event[1]==sdlconst.K_SPACE:
          print "space"
          self.set_state(idlers.PLAYING) 
  
  def begin_round_finished(self):
    self.fireworks.begin_round() 
    self.camera.begin_round()
    character.speed.x=character.speed.y=character.speed.z=0.
    character.play_animation(ANIM_JUMP)
    character.on_ground=1
        
    for event in soya.process_event():
      if event[0] == sdlconst.QUIT:
        sys.exit()
      elif event[0] == sdlconst.KEYDOWN:
        if   (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          sys.exit() # Quit the game
        
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
        elif event[1]==sdlconst.K_SPACE:
          if self.game_rounds>=len(self.levels)-1:
            self.stop()
          else:
            self.set_state(idlers.WAITING)
 
class FrictionVolume(soya.Volume):
  def __init__(self,parent,shape=None):
    soya.Volume.__init__(self,parent,shape)
    self.friction=0.0  
    self.ydamp=0.01
 
class Bonus(soya.Volume):
  def __init__(self,parent,shape=None):
    soya.Volume.__init__(self,parent,shape)
    self.points=0
    self.power=5.

  def advance_time(self,proportion):
    self.rotate_lateral(5.0*proportion)

class Target(FrictionVolume):
  def __init__(self,parent,shape=None):
    FrictionVolume.__init__(self,parent,shape)
    self.points=0
    self.name='Target'

def move_widgets():
  y=camera.height-140
  height=30
  color=(.4,.4,.9,.7)

  speed_label.top=y
  speed_label.width=640
  y+=height

  altitude_label.top=y
  altitude_label.width=640
  y+=height

  power_label.top=y
  power_label.width=640
  y+=height
  
  score_label.width=640
  score_label.top=y
  
def run(levelset=None,onelevel=None,commandline=False):
  global scene
  global level
  global snow
  global wind

  if not commandline:
    soya.IDLER.stop()

  # Create the scene (a world with no parent)
  scene = soya.World()

  level=None

  wind=soya.Vector(scene,random.random()-.5,random.random()-.5,random.random()-.5)/2.

  # adds our snow particle emmitter
  snow=particles.Snow(scene)

  # Creates and run an "idler" (=an object that manage time and regulate FPS)
  # By default, FPS is locked at 40.

  print "Starting Idler..."
  Idler(scene,levelset,onelevel).idle()  

  import highscore
  highscore.add_score(character.score)
  
  if not commandline:
    menu.MenuIdler().idle()
    

if __name__=='__main__':
  import optparse

  parser=optparse.OptionParser()
  parser.add_option("-l","--level",dest="level", help="level to load",metavar="LEVEL",default=None)
  parser.add_option("-s","--levelset",dest="levelset", help="levelset to load",metavar="LEVELSET",default='levelset1')
  parser.add_option("-o","--loop",dest="loop",action="store_true", help="loop level/levelset",default=False)

  options,args=parser.parse_args()
  print options, args

  if not options.level and not options.levelset:
    parser.print_help()
    sys.exit()
  
  soya.init(title="Snowball Surpise: Flying!", width=1024,height=768)

  if options.loop:
    while 1:
      run(options.levelset,options.level,commandline=True)
  else:
    run(options.levelset,options.level,commandline=True)
    
 
