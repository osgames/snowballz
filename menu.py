import sys, os, os.path

import random

import soya
import soya.ray
import soya.particle
from soya import widget 
from soya import sdlconst

try:
  from soya import openal4soya as openal
  SOUND=1
except:
  SOUND=0
  
from soya.opengl import *
from soya import sdlconst as sdl

try:
  from elementtree import ElementTree
except:
  print "[ERR] You do not have elementtree installed." 
  print "[ERR] Download it at http://effbot.org/downloads/#elementtree"
  sys.exit()

import racing
import flying

# we subclass this so we can store our descriptions as well
class ChoiceDesc(widget.Choice):
  def __init__(self,label='',action=None,description='',value=None,range=None,incr=None,data=None):
    widget.Choice.__init__(self,label,self._action,value,range,incr)
    self.description=description
    self.data=data
    self.real_action=action

  def _action(self):
    if self.real_action:
      self.real_action(*self.data)

# subclass ChoiceList so we can detect when the cursor moves
# unfortunatly we must replace almost all the class..
class AnimatedChoiceList(widget.ChoiceList):
  def __init__(self, master = None, choices = [], font = widget.default_font, color = (1.0, 0.5, 0.5, 1.0), highlight = (1.0, 1.0, 0.0, 1.0), cancel = None):
    widget.ChoiceList.__init__(self, master,choices,font,color,highlight,cancel)

    # create another snowflake to use as an extra highlight
    """
    self.snowflake = RotatingVolume(scene,lateral=0.,incline=10.,vertical=10.)
    snowflake=self.snowflake
    snowflake.set_shape(soya.Shape.get("snowflake"))
    snowflake.scale(0.01,0.01,0.01)
    snowflake.rotate_incline(105.0)
    """

  def on_change(self):
    # this is where we want to update the label and play a sound
    # you might also want to change the snowflake object or change 
    # a picture of something 
    global desclabel
    
    # get the position of the snowflake ( which is the position of the selected it ) and 
    # play the sound from there. ( this isnt very good )
    
    #p=self.snowflake.position()
    #p.z=3.0

    # play the sound 
    if SOUND:
      openal.play('flamingo.wav')

    # update the text of our description label
    soya.IDLER.desclabel.text=self.choices[self.selected].description

    # randomize the spin a bit 
    #self.snowflake.rotate_incline(random.randint(0,45))
    #self.snowflake.rotate_vertical(random.randint(0,45))
    #self.snowflake.rotate_lateral(random.randint(0,45))

  def set_selected(self,index):
    if index!=self.selected:
      self.selected=index
      if (self.selected>= len(self.choices)): self.selected=0
      if (self.selected < 0): self.selected = len(self.choices) - 1
      self.on_change()

  def get_selected(self):
    return self.selected

  def process_event(self, event):
    if (event[0] == soya.sdlconst.KEYDOWN):
      self.key_down(event[1], event[2])
    elif (event[0] == soya.sdlconst.MOUSEMOTION):
      self.mouse_move(event[1], event[2])
    elif (event[0] == soya.sdlconst.MOUSEBUTTONDOWN):
      self.choices[self.get_selected()].mouse_click(event[1])
    elif (event[0] == soya.sdlconst.JOYAXISMOTION):
      if event[1] == 0:
        if event[2] < 0:
          self.key_down(soya.sdlconst.K_LEFT, 0)
        elif event[2] > 0:
          self.key_down(soya.sdlconst.K_RIGHT, 0)
      elif event[1] == 1:
        if event[2] < 0:
          self.key_down(soya.sdlconst.K_UP, 0)
        elif event[2] > 0:
          self.key_down(soya.sdlconst.K_DOWN, 0)
    elif (event[0] == soya.sdlconst.JOYBUTTONDOWN):
      if event[1] // 2 == event[1] / 2.0:
        self.key_down(soya.sdlconst.K_RETURN, 0)
      else:
        self.key_down(soya.sdlconst.K_LEFT, 0)
        
  def key_down(self, key_id, mods):
    if key_id == soya.sdlconst.K_DOWN:
      self.set_selected(self.get_selected() + 1)
    elif key_id == soya.sdlconst.K_UP:
      self.set_selected(self.get_selected() - 1)
    elif key_id == soya.sdlconst.K_ESCAPE :
      if self.cancel:
        self.cancel.key_down(soya.sdlconst.K_RETURN, mods)
    else: self.choices[self.get_selected()].key_down(key_id, mods)
    
  def mouse_move(self, x, y):
    if (x >= self.left and x <= self.left + self.width):
      i = 0
      nb = len(self.choices)
      h1 = int (self.height / nb)
      h2 = int (h1 * 0.5)
      t = int (self.top + h1 * 0.5)
      while (i < nb):
        if (y >= t - h2 and y <= t + h2):
          self.set_selected( i)
          break
        t = t + h1
        i = i + 1
        
  def render(self):
    global camera
    global scene
    
    if self.visible:
      nb = len (self.choices)
      hi = self.height / nb
      h = int (self.top + hi * 0.5) - 18
      i = 0
      while (i < nb):
        if (i == self.get_selected()):
          glColor4f(*self.highlight)
        else:
          glColor4f(*self.color)

        #glDisable(GL_LIGHTING)
        self.font.draw_area(self.choices[i].get_label(), self.left, h, 0.0, self.width, h + hi,0) 
        h = h + hi
        i = i + 1

class MenuIdler(soya.Idler):
  # font to use for the menu
  menufont=soya.Font(os.path.join(os.getcwd(),"data","fonts","SNOW4.TTF"),40,30)
  descfont=soya.Font(os.path.join(os.getcwd(),"data","fonts","SNOW4.TTF"),30,15)

  menucolor=          (0.0, 0.0, 0.0, 0.5)
  menuhighlightcolor= (0.8, 3.0, 3.0, 0.5)
  desccolor=          (1.5, 1.5, 1.5, 0.8)

  def begin_round(self):
    for event in soya.process_event():
      if event[0]==sdlconst.VIDEORESIZE:
        self.move_widgets()
      elif event[0]==sdlconst.KEYDOWN and event[1]==sdlconst.K_ESCAPE:
        self.stop()
      elif event[0]==sdlconst.QUIT:
        self.stop()
      else:
        self.choicelist.process_event(event)

    soya.Idler.begin_round(self)

  def load_back(self):
    self.load_menu(self.lastmenu)

  def load_menu(self,file='mainmenu.xml'):
    self.lastmenu=self.currentmenu
    self.currentmenu=file

    path=os.path.normpath('data/xml/menu/'+file)
    menu=ElementTree.parse(path)
    root=menu.getroot()

    iter=root.getchildren()

    choices=[]

    for element in iter:
      action=None
      dest=None
      data=tuple()

      if element.attrib.has_key('loadmenu'):
        dest=element.get('loadmenu')
        action=self.load_menu
        data=(dest,)
      #elif element.attrib.has_key('website'):
      #  dest=element.get('website')
      #  action=lambda: menucall('website '+dest)
      elif element.attrib.has_key('func'):
        func=element.get('func')
        module=element.get('module',None)
        
        if func=='exit':
          action=sys.exit
        elif func=="back":
          action=self.load_back
        elif module=="racing" and func=="run":
          action=racing.run
          data=(element.get('levelset',None),element.get('level',None))
        elif module=="flying" and func=="run":
          action=flying.run
          data=(element.get('levelset',None),element.get('level',None))
         
      c=ChoiceDesc(element.text,action,element.get('desc',''),data=data)
      choices.append(c)
   
    self.choicelist.choices=choices

  def move_widgets(self):
    self.image.width=self.camera.width
    self.image.height=self.camera.height

    self.choicelist.height=soya.root_widget.height-220
    self.choicelist.width=soya.root_widget.width-100
    
    self.desclabel.width=soya.root_widget.width
    self.desclabel.top=soya.root_widget.height-120
    
    self.title.width=soya.root_widget.width
    self.title.top=soya.root_widget.height-60

  def reset(self):
    # Creates a camera.
    self.camera=camera= soya.Camera(self.scene)
    camera.z = 3.0

    root=widget.Group()
    soya.set_root_widget(root)
    soya.root_widget.add(camera)

    # load a font with a specific height and width attributes
    font=soya.Font(os.path.join(os.getcwd(),'data',"fonts","SNOW4.TTF"),50,30)

    # use the image as a full screen overlay 
    self.image_materail=material=soya.Material()
    material.texture=soya.Image.get('menuback.png')
    self.image=image=widget.Image(soya.root_widget,material)
    image.top=0
    image.left=0

    # create a basic label
    self.title=label=widget.Label(soya.root_widget,"Snowball Surprise!",font=font,align=1)

    # create a frames per second label
    self.fpslabel=fpslabel=widget.FPSLabel(soya.root_widget)
    fpslabel.left=0
    fpslabel.top=0

    # the label we will put the description of the menu item on 
    self.desclabel=desclabel=widget.Label(soya.root_widget,"Snowball Surprise!",align=1,font=self.descfont,color=self.desccolor)

    # choice list
    self.choicelist=choicelist=AnimatedChoiceList(soya.root_widget,font=self.menufont,color=self.menucolor,highlight=self.menuhighlightcolor)
    # make the widget the same width as the screen as items are centered
    choicelist.left=50
    choicelist.top=80

    self.move_widgets()

  def __init__(self,*scenes):
    # Creates the scene.
    self.scene = soya.World()

    soya.Idler.__init__(self,self.scene)

    self.reset()

    self.lastmenu=None
    self.currentmenu=None
   
    choices=self.load_menu()

    if SOUND:
      openal.init(self.camera)

      soundfile='chemical120j2.wav'
      music=openal.preload_sound(soundfile)

      self.soundloop=openal.play(soundfile,looping=1)

  def stop(self):
    soya.Idler.stop(self)
    if SOUND: self.soundloop.stop()

if __name__=='__main__':
  
  import optparse

  parser=optparse.OptionParser()
  parser.add_option("-x","--width",dest="width", help="screen x res",default=640)
  parser.add_option("-y","--height",dest="height", help="screen y res",default=480)
  parser.add_option("-f","--fullscreen",dest="fullscreen",action="store_true",help="run in fullscreen mode",default=False)

  options,args=parser.parse_args()

  width=int(options.width)
  height=int(options.height)

  if options.fullscreen:
    fullscreen=1
  else:
    fullscreen=0

  soya.init(title="Snowball Surprise!",width=width,height=height,fullscreen=fullscreen)
  soya.path.append(os.path.join(os.path.dirname(sys.argv[0]), "data"))

  print "you can pass options to this script!"

  MenuIdler().idle()


