import os

import soya

import Image
import ImageOps

import font

from elementtree import ElementTree

# waiting for the player to accept start
WAITING=0
# pre gameplay, moving camera etc
STARTING=1
# the game has just started
STARTED=2
# the game is in full swing 
PLAYING=3
# the game has been paused
PAUSED=4
# the player has lost 
LOST=5
# the player has one 
WON=6
# waiting for the player to accept end 
FINISHED=7

asstrings=['waiting',
        'starting',
        'started',
        'playing',
        'paused',
        'lost',
        'won',
        'finished',
        ]

class GameIdler(soya.Idler):
  def start_waiting(self): pass
  def start_starting(self): pass
  def start_started(self): pass
  def start_playing(self): pass
  def start_paused(self): pass
  def start_lost(self): pass
  def start_won(self): pass
  def start_finished(self): pass
  
  def end_waiting(self): pass
  def end_starting(self): pass
  def end_started(self): pass
  def end_playing(self): pass
  def end_paused(self): pass
  def end_lost(self): pass
  def end_won(self): pass
  def end_finished(self): pass
  
  def begin_round_waiting(self):              self.begin_round_default()
  def begin_round_starting(self):             self.begin_round_default()
  def begin_round_started(self):              self.begin_round_default()
  def begin_round_playing(self):              self.begin_round_default()
  def begin_round_paused(self):               self.begin_round_default()
  def begin_round_lost(self):                 self.begin_round_default()
  def begin_round_won(self):                  self.begin_round_default()
  def begin_round_finisehd(self):             self.begin_round_default()
  def begin_round_default(self):
    soya.Idler.begin_round(self)

  def advance_time_waiting(self,p):           self.advance_time_default(p)
  def advance_time_starting(self,p):          self.advance_time_default(p)
  def advance_time_started(self,p):           self.advance_time_default(p)
  def advance_time_playing(self,p):           self.advance_time_default(p)
  def advance_time_paused(self,p):            self.advance_time_default(p)
  def advance_time_lost(self,p):              self.advance_time_default(p)
  def advance_time_won(self,p):               self.advance_time_default(p)
  def advance_time_finisehd(self,p):          self.advance_time_default(p)
  def advance_time_default(self,p):
    soya.Idler.advance_time(self,p)

  def __init__(self,scene):
    print "Init Idler..."

    soya.Idler.__init__(self,scene)

    self.begin_round=self.begin_round_waiting
    self.advance_time=self.advance_time_waiting

    self.state=WAITING

  def set_state(self,state): 
    print "old state %s new state %s" % (asstrings[self.state],asstrings[state])

    if self.state==WAITING:
      self.end_waiting()
    elif self.state==STARTING:
      self.end_starting()
    elif self.state==STARTED:
      self.end_started()
    elif self.state==PLAYING:
      self.end_playing()
    elif self.state==PAUSED:
      self.end_paused()
    elif self.state==LOST:
      self.end_lost()
    elif self.state==WON:
      self.end_won()
    elif self.state==FINISHED:
      self.end_finished()

    self.state=state
    
    if self.state==WAITING:
      self.start_waiting()
      self.begin_round=self.begin_round_waiting
      self.advance_time=self.advance_time_waiting
    elif self.state==STARTING:
      self.start_starting()
      self.begin_round=self.begin_round_starting
      self.advance_time=self.advance_time_starting
    elif self.state==STARTED:
      self.start_started()
      self.begin_round=self.begin_round_started
      self.advance_time=self.advance_time_started
    elif self.state==PLAYING:
      self.start_playing()
      self.begin_round=self.begin_round_playing
      self.advance_time=self.advance_time_playing
    elif self.state==PAUSED:
      self.start_paused()
      self.begin_round=self.begin_round_paused
      self.advance_time=self.advance_time_paused
    elif self.state==LOST:
      self.start_lost()
      self.begin_round=self.begin_round_lost
      self.advance_time=self.advance_time_lost
    elif self.state==WON:
      self.start_won()
      self.begin_round=self.begin_round_won
      self.advance_time=self.advance_time_won
    elif self.state==FINISHED:
      self.start_finished()
      self.begin_round=self.begin_round_finished
      self.advance_time=self.advance_time_finished

class SnowballzIdler(GameIdler):
  def __init__(self,scene,levelset=None,onelevel=None):
    GameIdler.__init__(self,scene)

    self.scene=scene

    self.game_rounds=-1

    if onelevel:
      self.load_level(onelevel)
    elif levelset:
      self.load_levelset(levelset)
    else:
      raise "Nothing to load!"
  
    self.total_score=0

    self.set_state(WAITING)

  def load_level(self,fn="jumplevel"):
    self.levels=[fn]

  def load_levelset(self,fn="levelset1"):
    self.levels=[]
    
    self.levelset_tree=ElementTree.parse(os.path.join(HERE,'data','xml','flying',fn+'.xml'))
    self.levelset_root=root=self.levelset_tree.getroot()

    levels=root.findall('level')

    for level in levels:
      l= level.get('get','')
      self.levels.append(l)

  def stop(self):
    idlers.GameIdler.stop(self)

    for l in self.levels:
      del l

  def make_map_widget(self):
    im=Image.open(os.path.join('data','images','map2.png'))
    im=ImageOps.equalize(im)
    im=im.convert("RGBA")
    
    im2=Image.new("RGBA",im.size,(0.,0.,0.,0.,))
    im3=Image.blend(im,im2,.5)
    pixels=soya.image_from_pil(im3)

    map_image=soya.widget.Image(soya.root_widget,soya.Material(texture=pixels))
    map_image.left=0
    map_image.top=0
    map_image.width=256
    map_image.height=256

    self.mappointer=soya.widget.Image(soya.root_widget,soya.Material(soya.Image.get('littlestar.png')))
    self.mappointer.left=0
    self.mappointer.top=0
    self.mappointer.width=15
    self.mappointer.height=15

  def make_compass_widget(self):
    img=soya.Image.get('compass.png')
    self.compass=soya.widget.Image(soya.root_widget,soya.Material(texture=img))
    self.compass.solid=0
    self.compass.left=885
    self.compass.top=10
    self.compass.width=128
    self.compass.height=128

  def make_text_widgets(self):
    #color=(.4,.4,.9,.7)
    color=(1.,1.,1.,1.)

    self.speed_label=soya.widget.Label(soya.root_widget,font=font.label,color=color)
    self.altitude_label=soya.widget.Label(soya.root_widget,font=font.label,color=color,text="1")
    self.power_label=soya.widget.Label(soya.root_widget,font=font.label,color=color)
    self.score_label=soya.widget.Label(soya.root_widget,font=font.label,color=color,text="pts: 0")

  def make_overlay(self):
    self.overlay=overlay=soya.widget.Image(soya.root_widget,soya.Material(texture=soya.Image.get('gameoverlay.square.png')))
    overlay.left=0
    overlay.top=0
    overlay.width=self.camera.width
    overlay.height=self.camera.height
  
  def make_title(self):
    self.title_label=soya.widget.Label(soya.root_widget,text=self.level.levelname,font=font.normal,color=(1., 1., 1., .6,),align=1)
    self.title_label.width=self.camera.width
    self.title_label.height=400
    self.title_label.top=self.camera.height/3 

  def move_widgets(self):
    y=self.camera.height-140
    height=30
    color=(.4,.4,.9,.7)

    left=20

    self.speed_label.top=y
    self.speed_label.left=left
    self.speed_label.width=640
    y+=height

    self.altitude_label.top=y
    self.altitude_label.width=640
    self.altitude_label.left=left
    y+=height

    self.power_label.top=y
    self.power_label.width=640
    self.power_label.left=left
    y+=height
    
    self.score_label.width=40
    self.score_label.left=left
    self.score_label.top=y

