import soya, soya.sdlconst
#from soya.opengl import *

import math

import bezeir

class Eye(soya.World):  
  def __init__(self,parent = None): #Eye.__init__
    soya.World.__init__(self,parent)
    
#    self.eyeballsize = .8 #The size of the eyeball in relation to the eyelid.

    #self.topEyelidAngle = 120.0
    #self.bottomEyelidAngle = 240.0
    
    
    #self.targetTopEyelidAngle = 120.0
    #self.targetBottomEyelidAngle = 240.0
    #self.blinkTargetAngle = 190
    #self.blinkSpeed = .2
    
    #self.blinking = False
    #self.blinkInterval = 100.0
    #self.nextBlink = self.blinkInterval
    #self.blinkPos = 0.0
    
    #self.lid_material = DEFAULT_MATERIAL
    self.material = soya.DEFAULT_MATERIAL
    
    #self.lookDirection = soya.Vector(self,0.0,0.0,1.0)
    
    self.bezeircircle = None#bezeircircle
    self.crossbezeir = None#crossbezeir
    
    #self.setres(10,5)
    
    self.segments_updated = False #This is to signify if the segments need updating.
    self.segmentnum = 0
    self.ringnum = 0
    self.vertex_list = []
    
    
  #def batch(self):
  #  print "Batch"
  #  return 1, self, self.material  
  
  def set_res(self,segmentnum=16,ringnum=8):
    self.segmentnum = segmentnum
    self.ringnum = ringnum
    for i in range(segmentnum):
        vl = []
        for j in range(ringnum):
            v = soya.Vertex(self)
            #TODO texture coords
            vl.append(v)
        self.vertex_list.append(vl)
  
    self.sync_segments()
      
    #Now, we will build the faces.
    for i in range(segmentnum):
        vl = self.vertex_list[i]
        #print vl
        nvl = self.vertex_list[0]
        if i != segmentnum-1:
            nvl = self.vertex_list[i+1]
        for j in range(ringnum-1):
            if vl[j].x == 0 and vl[j].y == 0:
                soya.Face(self, [vl[j], nvl[j+1],vl[j+1]])
                print "Here"
            else:
                soya.Face(self, [vl[j], nvl[j], nvl[j+1], vl[j+1]])
            #print [vl[j], nvl[j], nvl[j+1], vl[j+1]]
    
  def sync_segments(self):
    for i in range(self.segmentnum):
        p = self.bezeircircle.point(float(i)/self.segmentnum)
        for cbp in self.crossbezeir.points[1:4]:
            cbp.x = p.x
            cbp.y = p.y
        vl = self.vertex_list[i]
        for j in range(self.ringnum):
            p = self.crossbezeir.point(float(j)/self.ringnum)
            vl[j].x = p.x
            vl[j].y = p.y
            vl[j].z = p.z
    self.segments_updated = True
    
    
  def render(self):
      if not self.segments_updated:
          self.sync_segments()
      print "Rendering!"
      soya.World.render(self)
  #try:
  #    self.material.activate()
      
  #    soya.World.render(self)
      
  def advance_time(self, proportion):
    #Check if we need to blink
    #self.nextBlink -= proportion
    #if self.nextBlink < 0:
    #  self.nextBlink = self.blinkInterval
    #  self.blinking = True
    #  
    #
    #if self.blinking:
    #  self.blinkPos += proportion*self.blinkSpeed
    #  if self.blinkPos >= 1.0:
    #    self.blinking = False
    #    self.blinkPos = 1.0
    #else:
    #  self.blinkPos -= proportion*self.blinkSpeed*.8
    #  if self.blinkPos < 0.0: self.blinkPos = 0.0
    #  
    #self.topEyelidAngle = (self.blinkTargetAngle-self.targetTopEyelidAngle)*self.blinkPos + self.targetTopEyelidAngle
    #self.bottomEyelidAngle = (self.blinkTargetAngle-self.targetBottomEyelidAngle)*self.blinkPos + self.targetBottomEyelidAngle
          
    #TEMPORARY
    self.rotate_lateral(proportion*2.0)  
    

def main():
  soya.init()
  scene = soya.World()
  eye = Eye(scene)
  eye.z = -1.0
  
  camera = soya.Camera(scene)
  camera.z = 2.0
  
  eye.rotate_lateral(0)
  
  soya.set_root_widget(camera)
  
  soya.toggle_wireframe()
  soya.Idler(scene).idle()
  #eye.render() 
 
if __name__ == "__main__": main()
