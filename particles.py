import soya
import soya.particle
import random

from math import sqrt

material_star=soya.Material()
material_star.additive_blending=1
material_star.texture=soya.Image.get('littlestar.png')

class Snow(soya.particle.Particles):
  def __init__(self,parent):
    soya.particle.Particles.__init__(self,parent,nb_max_particles=200)
    self.set_colors((0.1, 0.1, 0.1, 1.0), (0.3, 0.3, 0.3, 1.0), (0.3, 0.3, 0.3, 1.0), (0.1, 0.1, 0.1,0.1))
    self.set_sizes ((0.25, 0.25), (1.0, 1.0))
    self.auto_generate_particle=1

    self.width=40.
    self.depth=40.

  def generate(self, index):
    sx = random.random() - 0.5
    sy = random.random() + 1.0
    sz = random.random() - 0.5
    px = self.x+(random.random()*self.width*2)-self.width
    py = self.y+random.random()*4
    pz = self.z+(random.random()*self.depth*2)-self.depth
    l = (0.2 * (1.0 + random.random())) / sqrt(sx * sx + sy * sy + sz * sz) * .4
    self.set_particle2(index, random.random()*2.,px,py,pz, sx * l, -sy * l, sz * l, 0.0, -0.01, 0.0)

class PlayerParticles(soya.particle.Particles):
  def __init__(self,parent):
    soya.particle.Particles.__init__(self,parent,nb_max_particles=10)
    self.set_colors((0.1, 0.1, 0.1, 1.0), (0.3, 0.3, 0.3, 1.0), (0.3, 0.3, 0.3, 1.0), (0.1, 0.1, 0.1,0.1))
    self.set_sizes ((0.25, 0.25), (1.0, 1.0))
    self.auto_generate_particle=1

  def generate(self, index):
    sx = random.random() - 0.5
    sy = random.random() - 0.5
    sz = random.random() - 0.5
    l = (0.2 * (1.0 + random.random())) / sqrt(sx * sx + sy * sy + sz * sz) * .4
    self.set_particle(index, random.random()*4., sx * l, sy * l, sz * l, 0.0, 0., 0.0)

class BonusParticles(soya.particle.Particles):
  def __init__(self,parent,speed):
    soya.particle.Particles.__init__(self,parent,nb_max_particles=200,material=material_star)
    self.set_colors((0.9, 0.1, 0.1, 1.0), (0.9, 0.9, 0.1, 1.0), (0.9, 0.9, 0.1, 1.0), (0.9, 0.1, 0.1,0.1))
    #self.set_colors((.9, .9, .1, 1.), (1., 1., 1., 1.), (1., 1., 1., 1.), (.9, .9, .1, .1))
    self.set_sizes ((0.15, 0.15), (0.5, 0.5))

    self.cs=speed.copy()
    self.cs.convert_to(self)

  def generate(self, index):
    sx = (random.random() - 0.5) +self.cs.x
    sy = (random.random() - 0.5) +self.cs.y
    sz = (random.random() - 0.5) +self.cs.z
    l = (0.2 * (1.0 + random.random())) / sqrt(sx * sx + sy * sy + sz * sz) * 7.5
    self.set_particle(index, random.random()*4., sx * l, sy * l, sz * l, 0.,0.,0.)


