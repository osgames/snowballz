#!/usr/bin/python 

# little python script to resize images to something square
# NB. soya only likes textures ^2
# see --help for full options

import os
import sys

try:
  import Image
except:
  raise "You have not got PIL installed http://www.pythonware.com/downloads/index.htm"

def resize(input,output,size):
  im=Image.open(input)
  im=im.resize((size,size))
  im.save(output)

if __name__=="__main__":
  def die(msg):
    print "ERROR: %s\n" % msg
    parser.print_help()
    sys.exit()
  
  import optparse

  parser=optparse.OptionParser()
  parser.add_option("-f","--file",dest="filename", help="img to convert",metavar="FILE",default=None)
  parser.add_option("-o","--output",dest="output", help="output filename",metavar="FILE",default=None)
  parser.add_option("-x","--size",dest="size",help="size to make the ouput.(NB. soya wants textures ^2) default=512",default=512)

  options,args=parser.parse_args()

  if not options.filename:  die('you must specifiy an input filename')
  if not options.output:    die('you must specifiy an output filename')
  try: 
    options.size=int(options.size)
  except:
    die('size did not convert to an int')

  try:
    resize(options.filename,options.output,options.size)
  except Exception:
    raise Exception
  else:
    print "%s -> x%d -> %s OK" % (options.filename,options.size,options.output)

