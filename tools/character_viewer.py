import sys, os, os.path, soya
from soya import sdlconst

class ExplorerIdler(soya.Idler):
	def __init__(self,*scenes):
		soya.Idler.__init__(self,*scenes)

		self.speed=soya.Vector()
		self.grab_cursor=1
		self.roty=0.
		self.rotx=0.

	def idle(self):
		soya.clear_events()
		soya.Idler.idle(self)

	def begin_round(self):
		for event in soya.process_event():
			if event[0]==sdlconst.KEYDOWN:
				if event[1]==sdlconst.K_ESCAPE:
					self.stop()
				
				elif event[1]==sdlconst.K_UP:
					self.speed.z+=-.4
					if self.speed.z<-1.: self.speed.z=-1.
				elif event[1]==sdlconst.K_DOWN:
					self.speed.z+=.4
					if self.speed.z>1.: self.speed.z=1.
				
				elif event[1]==sdlconst.K_LEFT:
					self.speed.x+=-.2
					if self.speed.x<-1.: self.speed.x=-1.
				elif event[1]==sdlconst.K_RIGHT:
					self.speed.x+=.2
					if self.speed.x>1.: self.speed.x=1.

				elif event[1]==sdlconst.K_PAGEDOWN:
					self.speed.y+=-.2
					if self.speed.y<-1.: self.speed.y=-1.
				elif event[1]==sdlconst.K_PAGEUP:
					self.speed.y+=.2
					if self.speed.y>1.: self.speed.y=1.
		
				elif event[1]==sdlconst.K_SPACE:
					print "space!"
					self.speed.x=0.
					self.speed.y=0.
					self.speed.z=0.

				elif event[1]==sdlconst.K_F1:
					if self.grab_cursor==0:
						soya.cursor_set_visible(1)
						soya.set_grab_input(1)
						self.grab_cursor=1
					else:
						soya.cursor_set_visible(0)
						soya.set_grab_input(0)
						self.grab_cursor=0

			elif event[0]==sdlconst.KEYUP:
				if event[1]==sdlconst.K_UP:
					self.speed.z=0
					if self.speed.z<-1.: self.speed.z=-1.
				elif event[1]==sdlconst.K_DOWN:
					self.speed.z=0
					if self.speed.z>1.: self.speed.z=1.
				
				elif event[1]==sdlconst.K_LEFT:
					self.speed.x=0
				elif event[1]==sdlconst.K_RIGHT:
					self.speed.x=0
					if self.speed.x>1.: self.speed.x=1.

				elif event[1]==sdlconst.K_PAGEDOWN:
					self.speed.y=0
					if self.speed.y<-1.: self.speed.y=-1.
				elif event[1]==sdlconst.K_PAGEUP:
					self.speed.y=0
					if self.speed.y>1.: self.speed.y=1.

			elif event[0]==sdlconst.MOUSEMOTION:
				#event_type,mx,my,relx,rely,buttons=event

				rx=-float(event[3])/4.
				ry=-float(event[4])/6.
				
				if rx<1. and rx>-1.:
					rx=0.

				if ry<1. and ry>-1.:
					ry=0.

				rotx=self.rotx+rx
				
				if rotx>360. or rotx<-360.: rotx = rotx % 360.
				
				roty=self.roty+ry

				# constrain y rotation
				if roty<-90.:
					ry=0.
					roty=-90.

				if roty>90.:
					ry=0.
					roty=90.

				self.roty=roty
				self.rotx=rotx

				camera.turn_vertical(ry)
				camera.rotate_lateral(rx)


				#lblRotInfo.text="%.2f %.2f %.2f %.2f %.2f" % (self.rotx,self.roty,camera.x,camera.y,camera.z)

		soya.Idler.begin_round(self)

	def advance_time(self,proportion):
		#camera.add_vector(self.speed*proportion)
		camera.add_vector(soya.Vector(camera,0.0,0.0,-1.0)*proportion*-self.speed.z)
		soya.Idler.advance_time(self,proportion)


if __name__=='__main__':
  import optparse
  
  parser=optparse.OptionParser()
  parser.add_option("-s","--shape",dest="shape", help="cal3d shape to load",metavar="SHAPE",default="penguin")
  parser.add_option("-a","--action",dest="action", help="action to play",default="still")
  parser.add_option("-x","--scale",dest="scale",help="amount to scale by",default=1.0)
  parser.add_option("-l","--lights",dest="lights",help="light set to use. 1..n",default=1)
  parser.add_option("-f","--fog",dest="fog",action="store_true",help="turn on fog fog",default=False)

  options,args=parser.parse_args()

  soya.init()
  soya.path.append(os.path.normpath(os.path.join(os.path.dirname(sys.argv[0]), "../data")))

  scene = soya.World()
  print dir(scene)
    
  soya.cursor_set_visible(0)
  soya.set_grab_input(1)
  soya.set_mouse_pos(320,240)

  character=soya.Cal3dShape.get(options.shape)
  character_volume=soya.Cal3dVolume(scene,character)
  character_volume.animate_blend_cycle(options.action)
  character_volume.rotate_vertical(-90.0)

  if options.lights=="2":
    soya.Light(scene).set_xyz(0.5, 0.0, 2.0)
    soya.Light(scene).set_xyz(0.5, 0.0, -2.0)
  elif options.lights=="sun":
    sun = soya.Light(scene)
    sun.directional = 1
    sun.diffuse = (1.0, 0.8, 0.4, 1.0)
    sun.rotate_vertical(-45.0)
  else:
    soya.Light(scene).set_xyz(0.5, 0.0, 2.0)

  if options.fog:
    atmosphere = soya.SkyAtmosphere()
    atmosphere.ambient = (0.3, 0.3, 0.4, 1.0)
    atmosphere.fog = 1
    atmosphere.fog_type  = 0
    atmosphere.fog_start = 40.0
    atmosphere.fog_end   = 50.0
    atmosphere.fog_color = atmosphere.bg_color = (0.2, 0.5, 0.7, 1.0)
    atmosphere.skyplane  = 1
    atmosphere.sky_color = (1.5, 1.0, 0.8, 1.0)

    # Set the atmosphere to the level
    scene.atmosphere = atmosphere

  camera = soya.Camera(scene)
  camera.z = 3.0
  camera.look_at(character_volume)
  soya.set_root_widget(camera)

  ExplorerIdler(scene).idle()


