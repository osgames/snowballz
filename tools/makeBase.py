import os
import sys

HERE=os.path.dirname(sys.argv[0])
BASE=os.path.normpath(HERE+"/../base")

diagram=os.path.join(BASE,"diagrams","snowballz.dia")

"""
-rw-r--r--  1 dunk audio 10151 Jan 11 20:28 dia-uml.xsl
-rw-r--r--  1 dunk audio  9437 Jan 11 20:28 dia-uml2python.xsl
"""

dia_uml=os.path.join(HERE,"dia2python","dia-uml.xsl")
dia_uml2python=os.path.join(HERE,"dia2python","dia-uml2python.xsl")
temp="dia2python.temp"

os.system("xsltproc %s %s > %s" % (dia_uml,diagram,temp))

os.system("xsltproc --param directory \"'%s'\" %s %s" % ("base"+os.path.sep,dia_uml2python,temp))

print "All done"
