#normal modules
import os
import sys

import time

import webbrowser

from StringIO import StringIO

# "danger" modules
import soya
from soya import sdlconst
soya.path.append(os.path.join(os.getcwd(), "data"))

import wx
import wx.lib.mixins.listctrl as listmix
import wx.stc as stc
import wx.py as py

from elementtree.ElementTree import Element,SubElement,ElementTree,parse,dump

sys.path.append(os.getcwd())

# local modules
import LevelReader
import LevelXML

import snowballz
import flying

# define some common font names
# TODO: osx?

if wx.Platform == '__WXMSW__':
    faces = { 'times': 'Times New Roman',
              'mono' : 'Courier New',
              'helv' : 'Arial',
              'other': 'Comic Sans MS',
              'size' : 10,
              'size2': 8,
             }
else:
    faces = { 'times': 'Times',
              'mono' : 'Courier',
              'helv' : 'Helvetica',
              'other': 'new century schoolbook',
              'size' : 10.,
              'size2': 8.,
             }


# event when the attributes have been updated
EVT_ATTRIBUTE_UPDATE_ID=wx.NewId()

def EVT_ATTRIBUTE_UPDATE(win,func):
  win.Connect(-1,-1,EVT_ATTRIBUTE_UPDATE_ID,func)

class AttributeUpdateEvent(wx.PyEvent):
  def __init__(self):
    wx.PyEvent.__init__(self)
    self.SetEventType(EVT_ATTRIBUTE_UPDATE_ID)
    

class LevelListCtrl(wx.ListCtrl,
                    listmix.ListCtrlAutoWidthMixin,
                    listmix.TextEditMixin):
  def __init__(self,parent,level):
    wx.ListCtrl.__init__(self,parent,-1,style=wx.LC_REPORT|wx.LC_EDIT_LABELS|wx.LC_SINGLE_SEL|wx.LC_HRULES|wx.LC_VRULES)

    listmix.ListCtrlAutoWidthMixin.__init__(self)
    self.setupColumns()
    listmix.TextEditMixin.__init__(self)

    #self.make_button_editor()

    self.level=level

  def make_button_editor(self):
    # this doesnt work.. it should put a button instead of a edit over the cell 
    button=wx.PreButton()
    
    button.Create(self, -1)
    #button.SetBackgroundColour(wx.Colour(red=255,green=255,blue=175)) #Yellow
    button.SetFont(self.GetFont())
    button.SetLabel('button')

    button.Hide()
    self.button = button

    self.button.Bind(wx.EVT_LEFT_UP, self.onButtonClick)

  def onButtonClick(self,e):
    # this doesnt work.. it should put a button instead of a edit over the cell 
    dlg=wx.ColourDialog(self)
    dlg.GetColourData().SetChooseFull(True)
    if dlg.ShowModal()==wx.ID_OK:
      data=dlg.GetColourData()
      print data.GetColour().Get()

    dlg.Destroy()

  def setupColumns(self):
    self.InsertColumn(0,"Name")
    self.InsertColumn(1,"Type")
    self.InsertColumn(2,"Value")

  def refresh(self,data):
    self.DeleteAllItems()

    self.obj=data
    
    allowed= self.level.getAllowedAttributes(data.tag)
    attrs= self.level.combineAttributes(allowed,data.attrib)

    for (name,type,value) in attrs:
      id=self.InsertStringItem(wx.NewId(),name)
      self.SetStringItem(id,1,type)
      self.SetStringItem(id,2,str(value))

  def OpenEditor(self,col,row):
    x0 = self.col_locs[col]
    x1 = self.col_locs[col+1] - x0

    scrolloffset = self.GetScrollPos(wx.HORIZONTAL)

    # scroll foreward
    if x0+x1-scrolloffset > self.GetSize()[0]:
        if wx.Platform == "__WXMSW__":
            offset = x0+x1-self.GetSize()[0]-scrolloffset
            addoffset = self.GetSize()[0]/4
            if addoffset + scrolloffset < self.GetSize()[0]:
                offset += addoffset

            self.ScrollList(offset, 0)
            scrolloffset = self.GetScrollPos(wx.HORIZONTAL)
        else:
            self.CloseEditor()
            return

    y0 = self.GetItemRect(row)[1]
    
    type=self.GetItem(row,1).GetText()

    if type=="color":
      editor = self.button
    else:
      editor = self.editor
      editor.SetValue(self.GetItem(row, col).GetText())
      editor.SetSelection(-1,-1)

    editor.SetDimensions(x0-scrolloffset,y0, x1,-1)

    editor.Show()
    editor.Raise()
    editor.SetFocus()
    editor.Refresh()

    self.curRow = row
    self.curCol = col

  def CloseEditor(self,evt=None):
    listmix.TextEditMixin.CloseEditor(self,evt)

    name=self.GetItem(self.curRow,0).GetText()
    type=self.GetItem(self.curRow,1).GetText()
    value=self.GetItem(self.curRow,2).GetText()

    if type=="string":
      value=str(value)
    elif type=="float":
      value=float(value)
    elif type=="int":
      value=int(value)
    elif type=="boolean":
      value=int(value)

    self.obj.attrib[name]=value

    wx.PostEvent(self.GetParent().GetParent(),AttributeUpdateEvent())
 

class LevelTreeCtrl(wx.TreeCtrl):
  def __init__(self,parent,level):
    wx.TreeCtrl.__init__(self,parent,-1)

    self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)

    self.level=level
   
  def OnRightClick(self,evt):
    selection=self.GetSelection()
    data= self.GetPyData(selection)

    self.menu=wx.Menu()
    self.menu.Append(wx.NewId(),"Info")
  
    self.createMenu=wx.Menu()

    self.createMenuLookup={}
    
    for allowed in self.level.getAllowedElements(data.tag):
      menuitem=self.createMenu.Append(wx.NewId(),allowed)
      self.Bind(wx.EVT_MENU,self.OnMenu,id=menuitem.GetId())
      self.createMenuLookup[menuitem.GetId()]=allowed

    self.menu.AppendMenu(wx.NewId(),"Create",self.createMenu)

    self.PopupMenu(self.menu,evt.GetPosition())

  def OnMenu(self,evt):
    try:
      create=self.createMenuLookup[evt.GetId()]
    except IndexError:
      print "cannot create that"
      return

    selection=self.GetSelection()
    data=self.GetPyData(selection)
    
    node=SubElement(data,create)

    dump(data)

    self.refresh()

  def refresh(self):
    self.DeleteAllItems()
    self.tree_root=self.AddRoot(self.level.tree.getroot().tag)
    self.SetPyData(self.tree_root,self.level.tree.getroot())

    self.refresh_recur(self.level.tree.getroot(),self.tree_root)

  def refresh_recur(self,obj,parent):
    for child in obj.getchildren():
      print child
      node=self.AppendItem(parent,child.tag)
      self.SetPyData(node,child)

      self.refresh_recur(child,node)


# FIXME: rename this to something that doesnt clash with a game level to avoid confusion
class Level:
  def __init__(self):
    root=Element('level')

    self.tree=ElementTree(root)

    self.getElements()

  def load(self,filename):
    self.tree=parse(filename)
 
  def getScene(self):
    reader=LevelReader.FlyingLevelReader(elementtree=self.tree,level=snowballz.Level)
    wx.GetApp().setNewSceneLevel(reader.level)
    
  def getElements(self):
    self.desc_doc=parse(os.path.join('tools','LevelEditor.elements.xml'))
    self.elements={}

    for el in self.desc_doc.findall('/element'):
      name=el.get('name')

      assert(name)

      self.elements[name]=el

  def getAllowedElements(self,tagname):
    element=self.elements[tagname]
    ret=[]

    for el in element.findall('./el'):
      ret.append(el.text)      

    return ret
  
  def getAllowedAttributes(self,tagname):
    element=self.elements.get(tagname)

    if element==None:
      print "Nothing known about %s" % tagname
      return []
      
    ret=[]

    # do default attributes
    for at in element.findall('./atd'):
      type=at.get('type')

      if type=='pos':
        ret.append(('x',{'type':'float','default':0.00}))
        ret.append(('y',{'type':'float','default':0.00}))
        ret.append(('z',{'type':'float','default':0.00}))
      elif type=="scale":
        ret.append(('scale_x',{'type':'float','default':1.00}))
        ret.append(('scale_y',{'type':'float','default':1.00}))
        ret.append(('scale_z',{'type':'float','default':1.00}))
      elif type=="rot":
        ret.append(('lateral',{'type':'float','default':0.00}))
        ret.append(('vertical',{'type':'float','default':0.00}))
        ret.append(('incline',{'type':'float','default':0.00}))
        ret.append(('turn_lateral',{'type':'float','default':0.00}))
        ret.append(('turn_vertical',{'type':'float','default':0.00}))
        ret.append(('turn_incline',{'type':'float','default':0.00}))
      elif type=="color":
        ret.append(('r',{'type':'float','default':1.00}))
        ret.append(('g',{'type':'float','default':1.00}))
        ret.append(('b',{'type':'float','default':1.00}))
        ret.append(('a',{'type':'float','default':1.00}))
        #ret.append(('color',{'type':'color'}))

    # do normal attributes
    for at in element.findall('./at'):
      ret.append((at.text,at.attrib))      

    return ret

  def combineAttributes(self,allowed,actual):
    ret=[]

    for (name,attrib) in allowed:
      default=attrib.get('default','')
      type=attrib.get('type','string')
      value=actual.get(name,default)
      ret.append((name,type,value))

    return ret
  

class XMLEditorFrame(wx.Frame):
  class StyledTextCtrl(stc.StyledTextCtrl):
    def __init__(self,parent):
      stc.StyledTextCtrl.__init__(self,parent,style=stc.STC_STYLE_LINENUMBER)

      self.SetLexer(stc.STC_LEX_XML)
      
      self.SetMargins(0,0)
      self.SetViewWhiteSpace(False)
      
      self.SetEdgeMode(stc.STC_EDGE_BACKGROUND)
      self.SetEdgeColumn(78)

      self.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d" % faces)
      self.StyleClearAll()  # Reset all to be like the default

      # Global default styles for all languages
      self.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_STYLE_LINENUMBER,  "back:#C0C0C0,face:%(helv)s,size:%(size2)d" % faces)
      self.StyleSetSpec(stc.STC_STYLE_CONTROLCHAR, "face:%(other)s" % faces)
      self.StyleSetSpec(stc.STC_STYLE_BRACELIGHT,  "fore:#FFFFFF,back:#0000FF,bold")
      self.StyleSetSpec(stc.STC_STYLE_BRACEBAD,    "fore:#000000,back:#FF0000,bold")

      self.StyleSetSpec(stc.STC_H_DEFAULT,"fore:#000000,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_TAG,"fore:#cc0000,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_TAGUNKNOWN,"fore:#aa0000,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_ATTRIBUTE,"fore:#00cc00,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_ATTRIBUTEUNKNOWN,"fore:#00aa00,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_NUMBER,"fore:#000088,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_DOUBLESTRING,"fore:#008800,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_SINGLESTRING,"fore:#880000,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_OTHER,"fore:#101010,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_COMMENT,"fore:#303030,face:%(mono)s,size:%(size)d" % faces)
      self.StyleSetSpec(stc.STC_H_ENTITY,"fore:#ff0000,face:%(mono)s,size:%(size)d" % faces)
            
  def __init__(self,parent=None,id=-1,title="editor",size=(640,480),text=None):
    wx.Frame.__init__(self,parent,id,title,size=size)

    self.text=self.StyledTextCtrl(self)

    if text:
      self.text.SetText(text)

    self.sizer=wx.BoxSizer(wx.VERTICAL)
    self.sizer.Add(self.text,1,wx.EXPAND)

    self.SetSizer(self.sizer)
    self.SetAutoLayout(1)


class PyShellFrame(wx.Frame):
  def __init__(self,parent=None,id=-1,title="shell",size=(640,480)):
    wx.Frame.__init__(self,parent,id,title,size=size)

    self.shell=py.shell.Shell(self)

    sizer=wx.BoxSizer(wx.VERTICAL)
    sizer.Add(self.shell,1,wx.EXPAND)

    self.SetSizer(sizer)
    self.SetAutoLayout(1)


class MainFrame(wx.Frame):
  ACTION_NEW =wx.ID_NEW
  ACTION_OPEN=wx.ID_OPEN
  ACTION_CLOSE=wx.ID_CLOSE
  ACTION_SAVE=wx.ID_SAVE
  ACTION_SAVE_AS=wx.ID_SAVEAS
  ACTION_QUIT=wx.ID_EXIT
  ACTION_VIEW_XML=wx.NewId()
  ACTION_PY_SHELL=wx.NewId()
  ACTION_HELP=wx.NewId()

  def __init__(self,parent=None,id=-1,title="Main"):
    wx.Frame.__init__(self,parent,id,title,size=(300,600))

    self.CreateStatusBar()

    self.level=Level()

    fileMenu=wx.Menu()
    fileMenu.Append(self.ACTION_NEW,"New")
    fileMenu.Append(self.ACTION_OPEN,"Open")
    fileMenu.Append(self.ACTION_CLOSE,"Close")
    fileMenu.Append(self.ACTION_SAVE,"Save")
    fileMenu.Append(self.ACTION_SAVE_AS,"Save As...")
    fileMenu.AppendSeparator()
    fileMenu.Append(self.ACTION_QUIT,"Quit")

    viewMenu=wx.Menu()
    viewMenu.Append(self.ACTION_PY_SHELL,"Shell")
    viewMenu.Append(self.ACTION_VIEW_XML,"XML")

    helpMenu=wx.Menu()
    helpMenu.Append(self.ACTION_HELP,"View help")

    self.Bind(wx.EVT_CLOSE,self.OnCloseWindow)
    
    self.Bind(wx.EVT_MENU,self.onActionFileNew,id=self.ACTION_NEW)
    self.Bind(wx.EVT_MENU,self.onActionFileOpen,id=self.ACTION_OPEN)
    self.Bind(wx.EVT_MENU,self.onActionViewXML,id=self.ACTION_VIEW_XML)

    self.Bind(wx.EVT_MENU,self.onActionPyShell,id=self.ACTION_PY_SHELL)
    
    self.Bind(wx.EVT_MENU,self.onActionHelp,id=self.ACTION_HELP)

    menuBar=wx.MenuBar()
    menuBar.Append(fileMenu,"&File")
    menuBar.Append(viewMenu,"&View")
    menuBar.Append(helpMenu,"&Help")
    self.SetMenuBar(menuBar)

    self.splitter=wx.SplitterWindow(self,-1)

    self.tree=LevelTreeCtrl(self.splitter,self.level)
    self.Bind(wx.EVT_TREE_SEL_CHANGED,self.OnSelChanged)

    self.list=LevelListCtrl(self.splitter,self.level)
    EVT_ATTRIBUTE_UPDATE(self,self.OnAttributeUpdate)
    
    self.splitter.SplitHorizontally(self.tree,self.list)
    
    self.sizer=wx.BoxSizer(wx.VERTICAL)
    self.sizer.Add(self.splitter,1,wx.EXPAND)

    self.SetSizer(self.sizer)
    self.SetAutoLayout(1)

    self.refreshAll()

  def onActionHelp(self,evt):
    webbrowser.open("http://snowballz.literati.org/LevelEditor/help?action=show")
  
  def onActionPyShell(self,evt):
    win=PyShellFrame()
    win.Show(1)

  def onActionViewXML(self,evt):

    out=StringIO()

    self.level.tree.write(out)

    editor=XMLEditorFrame(text=out.getvalue())
    editor.Show(1)

  def onActionFileNew(self,evt):
    print "new"

  def onActionFileOpen(self,evt):
    dlg=wx.FileDialog(self,message="Choose a file",defaultDir=os.path.join('/home/dunk/cvs/snowballz','data','xml'),
                      defaultFile="",wildcard="*.xml",style=wx.OPEN|wx.CHANGE_DIR)

    if dlg.ShowModal() == wx.ID_OK:
      self.level.load(dlg.GetPath())
      self.refreshAll()
      self.level.getScene()

  def OnCloseWindow(self,evt):
    sys.exit()

  def OnAttributeUpdate(self,evt):
    self.level.getScene()

  def OnSelChanged(self,evt):
    selection=self.tree.GetSelection()
    data=self.tree.GetPyData(selection)

    self.list.refresh(data)

  def refreshAll(self):
    self.tree.refresh()
    selection=self.tree.GetSelection()
    data=self.tree.GetPyData(selection)
    self.list.refresh(data)
    
class LevelEditor(wx.App):
  def OnInit(self):
    self.frame=MainFrame()
    self.frame.Show(1)

    self.soyaIdler=None
    scene=self.makeBlankScene()
    self.initSoyaIdler(scene)

    return True
    
  def makeBlankScene(self):
    scene = soya.World()

    self.level=None
    
    self.camera = soya.Camera(scene)
    self.camera.z = 2.0
    self.camera.back=500.

    soya.set_root_widget(self.camera)

    return scene

  def setNewSceneLevel(self,level=None):
    if self.level:
      self.scene.remove(self.level)

    if level:
      self.level=level

    self.scene.add(self.level)

  def initSoyaIdler(self,scene=None):
    print "init soya idler"

    if scene!=None:
      print "setting scene"
      self.scene=scene

    ##### you cannot change the idler!!!!
    #FIXME/TODO: test threads more 
    if self.soyaIdler!=None and self.soyaIdler.running:
      self.soyaIdler.stop()
      time.sleep(2)

    self.soyaIdler=Idler(self)
    self.soyaIdler.idle()


class ExplorerIdlerWx(soya.Idler):
  XAXIS=0
  YAXIS=1
  ZAXIS=2
  NOAXIS=4

  def __init__(self,app):
    self.wxapp=app
    soya.Idler.__init__(self,app.scene)

    self.camera=wx.GetApp().camera

    self.speed=soya.Vector(self.camera)
    
    # are we grabbing the cursor?
    self.grab_cursor=0

    # are we zooming?
    self.zoom=0

    # are we dragging the selected object?
    self.drag=0

    # are we rotateing the selected object?
    self.rotating=0

    self.axis=self.XAXIS

    # camera rotation
    self.roty=0.
    self.rotx=0.

    self.selectedObject=None

    self.scene=app.scene

    self.stop_mouse_grab()

  def idle(self):
    # warping the mouse or init'ing sdl seems to cause a few to many
    # mouse events. this clears all the events that are currently in the
    # queue
    soya.clear_events()

    soya.Idler.idle(self)

  def select_object(self,obj=None):
    if obj!=None:
      self.selectedObject=obj
    else:
      self.selectedObject=None

  def on_key_down(self,event):
    if event[1]==sdlconst.K_ESCAPE:
      # i have always used sys.exit but i noticed that this is probably nicer
      self.stop()
    
    elif event[1]==sdlconst.K_w:
      self.speed.z+=-.8
    elif event[1]==sdlconst.K_s:
      self.speed.z+=.8

    elif event[1]==sdlconst.K_a:
      self.speed.x+=-.8
    elif event[1]==sdlconst.K_d:
      self.speed.x+=.8

    elif event[1]==sdlconst.K_LCTRL:
      self.zoom=1
    
    elif event[1]==sdlconst.K_SPACE:
      self.speed.x=0.
      self.speed.y=0.
      self.speed.z=0.

      #soya.Idler(scene).idle()

    elif event[1]==sdlconst.K_F1:
      if soya.get_grab_input()==0:
        soya.cursor_set_visible(0)
        soya.set_grab_input(1)
      else:
        soya.cursor_set_visible(1)
        soya.set_grab_input(0)
    elif event[1]==sdlconst.K_F2:
      print "save"
      level.save(options.output)

    elif event[1]==sdlconst.K_g and self.selectedObject:
      if self.drag==0:
        self.drag=1
        self.axis=self.NOAXIS
      else:
        self.drag=0

    elif event[1]==sdlconst.K_r and self.selectedObject:
      if self.rotating==0:
        self.rotating=1
      else:
        self.rotating=0

    elif event[1]==sdlconst.K_x:
      self.axis=self.XAXIS
    elif event[1]==sdlconst.K_y:
      self.axis=self.YAXIS
    elif event[1]==sdlconst.K_z:
      self.axis=self.ZAXIS
    elif event[1]==sdlconst.K_n:
      self.axis=self.NOAXIS

    elif event[1]==sdlconst.K_t:
      # see if we can pick something and then "teleport" to it 
      r=self.scene.raypick(self.camera.position(),soya.Vector(self.camera,0.0,0.0,-1.0))

      if r:
        self.camera.move(r[0]-soya.Vector(self.camera,0.,0.,-3.))

  def on_key_up(self,event):
    if event[1]==sdlconst.K_w:
      self.speed.z=0
      if self.speed.z<-1.: self.speed.z=-1.
    elif event[1]==sdlconst.K_s:
      self.speed.z=0
      if self.speed.z>1.: self.speed.z=1.

    elif event[1]==sdlconst.K_a:
      self.speed.x=0
    elif event[1]==sdlconst.K_d:
      self.speed.x=0

    elif event[1]==sdlconst.K_LCTRL:
      self.zoom=0
      self.speed.z=0.
    
  def on_mouse_button_down(self,event):
    if event[1]==2:
      self.start_mouse_grab()
    elif event[1]==1:
      mouse = self.camera.coord2d_to_3d(event[2], event[3])
      
      result = self.scene.raypick(self.camera, self.camera.vector_to(mouse))
      
      obj=None
      
      if result:
        obj = result[0].parent
       
      self.select_object(obj)

    return
    
    self.rotating=0
    self.drag=0

    if event[1]==1:
      self.speed.z=-.8
    if event[1]==3:
      # see if we can pick something and then "teleport" to it 
      context=scene.RaypickContext(self.camera.position(),50.)
      r=context.raypick(self.camera.position(),soya.Vector(self.camera,0.0,0.0,-1.0),50.0,3)
      
      if r:
        self.select_object(r[0].parent)
      else:
        self.select_object()

  def on_mouse_button_up(self,event):
    self.stop_mouse_grab()

    self.speed.z=0

  def start_mouse_grab(self):
    soya.cursor_set_visible(0)
    soya.set_grab_input(1)
    soya.set_mouse_pos(self.camera.width/2,self.camera.height/2)
    soya.clear_events()
    Rx,ry=soya.get_mouse_rel_pos()
    
    self.grab_cursor=1

  def stop_mouse_grab(self):
    self.grab_cursor=0
    soya.cursor_set_visible(1)
    soya.set_grab_input(0)

  def on_mouse_motion(self,event):
    if not self.grab_cursor:
      return
    
    #event_type,mx,my,relx,rely,buttons=event

    # scale the rotation amount by some arbitary amounts 
    rx=-float(event[3])/4.
    ry=-float(event[4])/6.

    # my mouse is too sensitive!
    if rx<1. and rx>-1.:
      rx=0.

    if ry<1. and ry>-1.:
      ry=0.

    if self.zoom==1:
      self.speed.z-=ry/10.
    
    elif self.drag==1:
      if not self.selectedObject: return 

      if self.axis==self.NOAXIS:
         self.selectedObject.add_vector(soya.Vector(self.camera,-rx/2.,ry/2.,0.))
      elif self.axis==self.XAXIS:
         self.selectedObject.add_vector(soya.Vector(self.camera,-rx/2.,0.,0.))
      elif self.axis==self.YAXIS:
         self.selectedObject.add_vector(soya.Vector(self.camera,0.,ry/2.,0.))
      elif self.axis==self.ZAXIS:
         self.selectedObject.add_vector(soya.Vector(self.camera,0.,0.,ry/2.))

    elif self.rotating==1:
      if not self.selectedObject: return 

      if self.axis==self.XAXIS:
        self.selectedObject.rotate_lateral(rx)
      elif self.axis==self.YAXIS:
        self.selectedObject.rotate_vertical(rx)
      elif self.axis==self.ZAXIS:
        self.selectedObject.rotate_incline(rx)
      
    else:  
      # constrain the rotation to <360
      rotx=self.rotx+rx
      if rotx>360. or rotx<-360.: rotx = rotx % 360.
      
      roty=self.roty+ry

      # constrain y rotation so we cant break our necks
      if roty<-90.:
        ry=0.
        roty=-90.

      if roty>90.:
        ry=0.
        roty=90.

      self.roty=roty
      self.rotx=rotx

      self.camera.turn_vertical(ry)
      self.camera.rotate_lateral(rx)

      # update our label 
      #lblRotInfo.text="%.2f %.2f %.2f %.2f %.2f" % (self.camera.x,self.camera.y,self.camera.z,self.rotx,self.roty)

  def resize(self):
    pass

  def event_filter(self,event):
    return False

  def begin_round(self):
    for event in soya.process_event():
      if self.event_filter(event):
        pass
      elif event[0]==sdlconst.QUIT:
        self.stop()
      elif event[0]==sdlconst.VIDEORESIZE:
        self.resize()
      elif event[0]==sdlconst.KEYDOWN:
        self.on_key_down(event)
      elif event[0]==sdlconst.KEYUP:
        self.on_key_up(event)
      elif event[0]==sdlconst.MOUSEBUTTONDOWN:
        self.on_mouse_button_down(event)
      elif event[0]==sdlconst.MOUSEBUTTONUP:
        self.on_mouse_button_up(event)
      elif event[0]==sdlconst.MOUSEMOTION:
        self.on_mouse_motion(event)
    
    #lblRotInfo.text="%.2f %.2f %.2f %.2f %.2f" % (self.camera.x,self.camera.y,self.camera.z,self.rotx,self.roty)
    soya.Idler.begin_round(self)

  def advance_time(self,proportion):
    soya.Idler.advance_time(self,proportion)
    self.camera.add_mul_vector(proportion,self.speed)
    self.wxapp.Yield()

class SelectionMarker(soya.PythonCoordSyst):
  def __init__(self,parent=None,material=None):
    soya.PythonCoordSyst.__init__(self,parent)

    self._material=material or soya.DEFAULT_MATERIAL

  def batch(self):
    return 2,self,None

  def render(self):
    glDisable(GL_CULL_FACE)
    glDisable(GL_DEPTH_TEST)
    glDisable(GL_LIGHTING)
    
    glColor4f(1.,0.,0.,1.)

    glBegin(GL_LINES)
    glColor4f(1.,0.,0.,1.)
    glVertex3f(0.,0.,0.)
    glVertex3f(1.,0.,0.)
    glColor4f(0.,1.,0.,1.)
    glVertex3f(0.,0.,0.)
    glVertex3f(0.,1.,0.)
    glColor4f(0.,0.,1.,1.)
    glVertex3f(0.,0.,0.)
    glVertex3f(0.,0.,1.)
    glEnd()

    glEnable(GL_LIGHTING)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)

class Idler(ExplorerIdlerWx):
  pass

if __name__=='__main__':
  print 
  print "----------------------------------------------------"

  soya.init()

  app=LevelEditor()
  app.MainLoop()
