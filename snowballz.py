#! /usr/bin/python -O

# standard modules

import sys, os, os.path 

import traceback

import random

import math 
from math import sqrt,atan,degrees,sin

# test for any non-standard modules
# try to give some nice instructions if things dont work 

try:
  import soya
  import soya.widget as widget
  import soya.sdlconst as sdlconst
  import soya.particle

  import soya.cube
  import soya.sphere

  from soya.opengl import *
  
  from soya import Vertex,Face
except ImportError:
  # if we do this then the user can see the -actual- error 
  traceback.print_exc()

  print 
  print "[ERR] There was an error loading Soya. This could mean that you do not have it installed."  
  print "[ERR] Download it from http://home.gna.org/oomadness/en/soya/index.html"
  sys.exit(1)

try:
  from elementtree import ElementTree
except ImportError:
  traceback.print_exc()

  print 
  print "[ERR] There was an error loading elementtree. This could mean that you do not have it installed."
  print "[ERR] Download it from http://effbot.org/downloads/#elementtree" 

try:
  import Image
  import ImageOps
except:
  traceback.print_exc()

  print 
  print "[ERR] You dont have PIL installed"
  print "[ERR] Download it from http://www.pythonware.com/downloads/index.htm"

try:
  from soya import openal4soya as openal
  SOUND=1
except:
  SOUND=0

HERE = os.path.dirname(sys.argv[0])
soya.path.append(os.path.join(HERE, "data"))

# we need to import ourself so that levels using flying.Bonus etc can be handled
import flying

# snowballz imports
import font
import menu
import player
import idlers
import particles

# globals 
scene=None
snow=None
level=None
wind=None

material_star=soya.Material()
material_star.additive_blending=1
material_star.texture=soya.Image.get('littlestar.png')

# The available actions
ACTION_WAIT          = 0
ACTION_ADVANCE       = 1
ACTION_ADVANCE_LEFT  = 2
ACTION_ADVANCE_RIGHT = 3
ACTION_TURN_LEFT     = 4
ACTION_TURN_RIGHT    = 5
ACTION_GO_BACK       = 6
ACTION_GO_BACK_LEFT  = 7
ACTION_GO_BACK_RIGHT = 8
ACTION_JUMP          = 9
ACTION_LOOK          = 10
ACTION_THROW         = 11

#ANIM_STILL="still"
ANIM_STILL="Stand_Still"
ANIM_SLIDE="Stand_Still"
ANIM_FLAP="Flap"
ANIM_JUMP="Jump"
ANIM_FALLING="Falling"
ANIM_WALK="Walk"

class Action:
  def __init__(self, action):
    self.action = action

class Level(soya.World):
  def __init__(self,parent=None,shape=None):
    soya.World.__init__(self,parent,shape)

    self.levelname="UN-Named"
    self.water=None
    self.land=None

    self.spawn_points=[]

    self.current_spawn=0

class DropVolume(soya.Volume):
  def __init__(self, parent = None, shape = None,mass=1.0):
    soya.Volume.__init__(self,parent,shape)
    self.mass=mass
    self.speed=soya.Vector(self)
    print self.speed
    self.radius=4.
    self.on_ground=0

    self.left   = soya.Vector(self, -1.0,  0.0,  0.0)
    self.right  = soya.Vector(self,  1.0,  0.0,  0.0)
    self.down   = soya.Vector(self,  0.0, -1.0,  0.0)
    self.up     = soya.Vector(self,  0.0,  1.0,  0.0)
    self.front  = soya.Vector(self,  0.0,  0.0, -1.0)
    self.back   = soya.Vector(self,  0.0,  0.0,  1.0)

    self.center = soya.Point(self, 0.0, self.radius, 0.0)
    self.solid=0
    
  # this way doesnt work as well
  """
  def put_on_land(self,land):
    p=soya.Point(land,self.x,self.y,self.z)
    h,v=land.get_true_height_and_normal(self.x/land.scale_factor,self.y/land.scale_factor)
    print h
    self.y=h+land.y
    #print land.y,self.y,lh,(lh*land.scale_factor)/land.multiply_height
  """

  def begin_round(self):
    soya.Volume.begin_round(self)

    if self.on_ground==0:
      new_center = self.center + self.speed
      context = soya.IDLER.scene.RaypickContext(new_center, self.radius)
      
      # Gets the ground, and check if the volume is falling
      r = context.raypick(new_center, self.down, self.radius, 3)

      if r and r[1].parent!=self:
        self.on_ground=1

        ground, ground_normal = r
        ground.convert_to(self)
        self.speed.y = ground.y
        self.solid=1
      else:
        self.on_ground=0
        self.speed.y = max(self.speed.y - 0.05, -0.45)
 
  def advance_time(self,p):
    if self.on_ground==0:
      self.add_mul_vector(p,self.speed)

    soya.Volume.advance_time(self,p)

class Snowball(soya.World):
  def __init__(self,parent,thrower):
    soya.World.__init__(self,parent,None)

    self.ball=soya.sphere.Sphere(self,slices=10,stacks=10,material=soya.Material.get('snow'),smooth_lit=1)

    self.has_hit=0
    
    #self.ball.scale(0.5,.5,.5)
    self.solid=0

    self.speed=soya.Vector(self,0.,thrower.speed.y+1.,thrower.speed.z-1.)

    self.radius=1.

    self.center = soya.Point(self, 0.0, self.radius, 0.0)
    
    self.left   = soya.Vector(self, -1.0,  0.0,  0.0)
    self.right  = soya.Vector(self,  1.0,  0.0,  0.0)
    self.down   = soya.Vector(self,  0.0, -1.0,  0.0)
    self.up     = soya.Vector(self,  0.0,  1.0,  0.0)
    self.front  = soya.Vector(self,  0.0,  0.0, -1.0)
    # unused
    #self.back   = soya.Vector(self,  0.0,  0.0,  1.0)

    self.move(soya.Point(thrower,1.5,2.,-1))
    self.look_at(thrower.front)

  def advance_time(self,p):
    if self.has_hit==0:
      self.add_mul_vector(p,self.speed)

  def hit(self,obj=None):
    self.has_hit=1
    self.parent.remove(self)
    print "hit obj",obj
    if SOUND:
      print "play hit"
      print type(obj)
      if type(obj)!=soya.Land:
        if obj.shape.filename=="bear.noanim":
          openal.play('chemical120j2.wav',position=self.center)
      else:
        openal.play('flamingo.wav',position=self.center)
      

  def begin_round(self):
    if self.has_hit:
      return 

    new_center = self.center + self.speed
    context = scene.RaypickContext(new_center, self.radius)
    
    # Gets the ground, and check if the character is falling
    r = context.raypick(new_center, self.down, 0.1 + self.radius, 3)

    if r:
      ground, ground_normal = r
      ground.convert_to(self)
      self.speed.y = ground.y
      self.hit(r[1].parent)
      return 
    else:
      if self.y<-100:
        self.hit()

      self.speed.y = max(self.speed.y - 0.05, -0.85)
      if self.speed.y < 0.0: self.flying = 0
 
    new_center = self.center + self.speed
    
    #for vec in (self.left, self.right, self.front, self.back, self.up):
    for vec in (self.left, self.right, self.front, self.up):
      r = context.raypick(new_center, vec, self.radius, 3)
      if r:
        collision, wall_normal = r
        self.hit(collision.parent)
        return 
        
class Water(soya.World):
  def __init__(self,parent=None,shape=None):
    soya.World.__init__(self,parent,shape)

    self.solid=0

    sz= parent.land.width*parent.land.scale_factor
 
    material=soya.Material(texture=soya.Image.get('water.png'))
 
    Face(self, [Vertex(self,  0.0,  0.0,  0.0, 1.0, 0.0),
              Vertex(self,  0.0,  0.0, sz, 1.0, 1.0),
              Vertex(self, sz,  0.0, sz, 0.0, 1.0),
              Vertex(self, sz,  0.0,  0., 0.0, 0.0),
              ],material).double_sided=1

    self.t=0.

  def advance_time(self,p):
    soya.World.advance_time(self,p)

    self.y-=sin(self.t)/10
    self.t+=.01
    if self.t>360.: self.t=0.
    
class FrictionVolume(soya.Volume):
  def __init__(self,parent,shape=None):
    soya.Volume.__init__(self,parent,shape)
    self.friction=0.0  
    self.ydamp=0.01
 
class Bonus(soya.Volume):
  def __init__(self,parent,shape=None):
    soya.Volume.__init__(self,parent,shape)
    self.points=0
    self.power=5.

  def advance_time(self,proportion):
    self.rotate_lateral(5.0*proportion)

class Target(FrictionVolume):
  def __init__(self,parent,shape=None):
    FrictionVolume.__init__(self,parent,shape)
    self.points=0
    self.name='Target'

class KeyboardControler:
  """A controler is an object that gives orders to a character.
Here, we define a keyboard based controler, but there may be mouse-based or IA-based
controlers.
Notice that the unique method is called "next", which allows to use Python generator
as controller."""
  def __init__(self):
    self.left_key_down = self.right_key_down = self.up_key_down = self.down_key_down = 0
    
  def next(self):
    """Returns the next action"""
    
    for event in soya.process_event():
      if event[0]==sdlconst.VIDEORESIZE:
        move_widgets()
      elif event[0]==sdlconst.QUIT:
        sys.exit()
      if event[0] == sdlconst.KEYDOWN:
        if   (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          soya.IDLER.stop() # Quit the game
        
        elif event[1] == sdlconst.K_LSHIFT:
          return Action(ACTION_JUMP)
          
        elif event[1] == sdlconst.K_a:     self.left_key_down  = 1
        elif event[1] == sdlconst.K_d:     self.right_key_down = 1
        elif event[1] == sdlconst.K_w:     self.up_key_down    = 1
        elif event[1] == sdlconst.K_s:     self.down_key_down  = 1
        elif event[1] == sdlconst.K_z:     
          return Action(ACTION_LOOK)
        elif event[1] == sdlconst.K_SPACE:
          return Action(ACTION_THROW)
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
        
      elif event[0] == sdlconst.KEYUP:
        if   event[1] == sdlconst.K_a:       self.left_key_down  = 0
        elif event[1] == sdlconst.K_d:       self.right_key_down = 0
        elif event[1] == sdlconst.K_w:       self.up_key_down    = 0
        elif event[1] == sdlconst.K_s:       self.down_key_down  = 0

      elif event[0] == sdlconst.MOUSEBUTTONDOWN:
        if event[1] == 1:
          return Action(ACTION_THROW)
        if event[1] == 3:
          return Action(ACTION_JUMP)
          

      """
      elif event[0] == sdlconst.MOUSEMOTION:
        #event_type,mx,my,relx,rely,buttons=event

        rx=-float(event[3])/6.
        ry=-float(event[4])/6.

        if rx<-.2: 
          self.right_key_down=1
          self.left_key_down=0
        elif rx>.2:
          self.right_key_down=0
          self.left_key_down=1
        else:
          self.right_key_down=0
          self.left_key_down=0
      """

        
    # People saying that Python doesn't have switch/select case are wrong...
    # Remember this if you are coding a fighting game !
    return Action({
      (0, 0, 1, 0) : ACTION_ADVANCE,
      (1, 0, 1, 0) : ACTION_ADVANCE_LEFT,
      (0, 1, 1, 0) : ACTION_ADVANCE_RIGHT,
      (1, 0, 0, 0) : ACTION_TURN_LEFT,
      (0, 1, 0, 0) : ACTION_TURN_RIGHT,
      (0, 0, 0, 1) : ACTION_GO_BACK,
      (1, 0, 0, 1) : ACTION_GO_BACK_LEFT,
      (0, 1, 0, 1) : ACTION_GO_BACK_RIGHT,
      }.get((self.left_key_down, self.right_key_down, self.up_key_down, self.down_key_down), ACTION_WAIT))

class Character(player.Player):
  """A character in the game."""
  def __init__(self, parent, controler):
    player.Player.__init__(self, parent,controler)

    self.particles=particles.PlayerParticles(parent)
   
    self.flying = 0
    self.swimming = 0 
    self.on_ground=0
    self.looking=0

    self.look_camera=None

    self.power=100.
    self.score=0
    self.bonus=0
    self.bonuscount=0
    self.target_points=0
    

  def do_it(self,angle,prop):
    air_density=0.000637
    speed=self.speed.z
   
    chord=4.
    span=8.
    planform=chord*span

    #drag
    cdf0=[0.01,0.0074,0.004,0.009,0.013,0.023,0.05,0.12,0.21]
    a=[-60.,-40.,0.,20.,40.,60.,80.,100.,120.]

    cd=0.5

    for i in range(0,8):
      if a[i]<=angle and a[i+1]>angle:
        cd=cdf0[i]-(a[i]-angle)*(cdf0[i]-cdf0[i+1]) / (a[i]-a[i+1])

    #lift
    clf0=[-.54, -.2, 0.2, .57, 0.92, 1.21, 1.43, 1.4, 1.0]

    cl=0.

    for i in range(0,8):
      if a[i]<=angle and a[i+1]>angle:
        cl=clf0[i]-(a[i]-angle)*(clf0[i]-clf0[i+1]) / (a[i]-a[i+1])


    drag=cd*0.5*air_density*(speed*speed)*planform
    lift=cl*0.5*air_density*(speed*speed)*planform

    print angle,"%.2f" % lift,drag,self.speed.z

    self.speed.y+=lift*prop
    self.speed.z+=drag*prop
    
    return lift,drag


  def got_bonus(self,obj):
    print "got bonus",obj,id(obj)
    self.score+=obj.points
    self.bonus+=obj.points
    self.bonuscount+=1
    obj.visible=0
    self.power+=obj.power
    score_label.text="pts: %d" %self.score

    particles=BonusParticles(self.parent)
    particles.move(obj)

  def got_target(self,obj):
    print "got target"
    self.speed.x=0.
    self.speed.y=0.
    self.speed.z=0.
    self.target_points=obj.points
    soya.IDLER.score('hit target!!',smallmsg=obj.name)

  def got_out(self):
    print "got out"
    self.speed.x=0.
    self.speed.y=0.
    self.speed.z=0.
    soya.IDLER.score('OUT!!',bad=True)
 
  def do_look(self):
    print "do look"

    if self.looking==0:
      print "lookin on"
      self.looking=1
      
      self.old_root_widget=soya.root_widget

      self.look_camera=ncamera = soya.Camera(scene)
      ncamera.move(character)
      ncamera.add_vector(soya.Vector(character,0,3,0))
      ncamera.back = 470.0
      
      self.visible=0

      ncamera.look_at(character.front)

      soya.set_root_widget(widget.Group())
      soya.root_widget.add(widget.FPSLabel())
      soya.root_widget.add(ncamera)

    else:
      self.looking=0

      self.visible=1

      soya.set_root_widget(self.old_root_widget)

  def do_throw(self):
    snowball=Snowball(soya.IDLER.level,self)

  def begin_round(self):
    if soya.IDLER.state==idlers.PLAYING:   
      self.begin_action(self.controler.next())

    soya.World.begin_round(self)
  
  def begin_action(self, action):
    if action.action==ACTION_LOOK:
      self.do_look()
      return 
    elif action.action==ACTION_THROW:
      self.do_throw()
      return 

    # Reset
    self.speed.x = self.speed.z = self.rotation_speed = 0.0

    mx,my=soya.get_mouse_rel_pos()
    self.rotation_speed=-mx/20
    soya.IDLER.camera.traveling.top_view+=my*.001
    if soya.IDLER.camera.traveling.top_view>0.8: soya.IDLER.camera.traveling.top_view=0.8
    if soya.IDLER.camera.traveling.top_view<-0.6: soya.IDLER.camera.traveling.top_view=-0.6

    # If the haracter is flying, we don't want to reset speed.y to 0.0 !!!
    if (not self.flying) and self.speed.y > 0.0: self.speed.y = 0.0
    
    if soya.IDLER.state!=idlers.PLAYING: return 
    animation = ANIM_STILL
    
    # Determine the character rotation
    if   action.action in (ACTION_TURN_LEFT, ACTION_ADVANCE_LEFT, ACTION_GO_BACK_LEFT):
      #self.rotation_speed = 4.0
      self.speed.x=-.5
      animation = ANIM_STILL
    elif action.action in (ACTION_TURN_RIGHT, ACTION_ADVANCE_RIGHT, ACTION_GO_BACK_RIGHT):
      #self.rotation_speed = -4.0
      self.speed.x=.5
      animation = ANIM_STILL
      
    # Determine the character speed
    if self.looking==0:
      if action.action in (ACTION_ADVANCE, ACTION_ADVANCE_LEFT, ACTION_ADVANCE_RIGHT):
        self.speed.z = -0.85
        animation = ANIM_WALK
      elif action.action in (ACTION_GO_BACK, ACTION_GO_BACK_LEFT, ACTION_GO_BACK_RIGHT):
        self.speed.z = 0.8
        animation = ANIM_WALK
        
    new_center = self.center + self.speed
    context = scene.RaypickContext(new_center, max(self.radius, 0.1 + self.radius_y))
    
    # Gets the ground, and check if the character is falling
    r = context.raypick(new_center, self.down, 0.1 + self.radius_y, 3)

    if r and not self.flying:
      # Puts the character on the ground
      # If the character is flying, we do not put him on the ground !
      ground, ground_normal = r
      ground.convert_to(self)
      self.speed.y = ground.y
      self.on_ground=1
      
      # flying is only possible if we are on ground
      if action.action == ACTION_JUMP and self.looking==0:
        self.flying = 1
        self.speed.y = 0.9
        self.perso.animate_execute_action(ANIM_JUMP)
       
    else:
      # No ground => start falling
      # Test the fall with the pit behind the second house
      if self.y<self.parent.water.y:
        self.swimming=1
        self.speed.y = max(self.speed.y - 0.02, -0.25)
      
        if action.action == ACTION_JUMP:
          self.flying=1
          self.speed.y = 0.7
      else:
        self.swimming=0
        self.speed.y = max(self.speed.y - 0.05, -0.85)

      animation = ANIM_FALLING
      
      # If the vertical speed is negative, the jump is over
      if self.speed.y < 0.0: self.flying = 0
      self.on_ground=0
      
    new_center = self.center + self.speed
    
    # The movement (defined by the speed vector) may be impossible if the character
    # would encounter a wall.
    
    for vec in (self.left, self.right, self.front, self.back, self.up):
      r = context.raypick(new_center, vec, self.radius, 3)
      if r:
        # The ray encounters a wall => the character cannot perform the planned movement.
        # We compute a correction vector, and add it to the speed vector, as well as to
        # new_center (for the following raypicks ; remember that
        # new_center = self.center + self.speed, so if speed has changed, we must update
        # it).
        
        collision, wall_normal = r
        hypo = vec.length() * self.radius - (new_center >> collision).length()
        correction = wall_normal * hypo
        
        # Theorical formula, but more complex and identical result
        #angle = (180.0 - vec.angle_to(wall_normal)) / 180.0 * math.pi
        #correction = wall_normal * hypo * math.cos(angle)
        
        self.speed.__iadd__(correction)
        new_center.__iadd__(correction)
        
    self.play_animation(animation)

  def advance_time(self, proportion):
    soya.World.advance_time(self, proportion)

    if self.flying and self.power>0.1:
      self.power-=5.*proportion

    # completely arbitary speed display
    soya.IDLER.speed_label.text="spd: %.2f" % (self.speed.length()*100)
    soya.IDLER.power_label.text="pow: %.2f" % (self.power)
    soya.IDLER.altitude_label.text="alt: %.2f" % (self.y)

    snow.move(self)
    self.particles.move(self)

    if soya.IDLER.state!=idlers.PLAYING: return 

    if self.on_ground==0:
      #self.add_mul_vector(proportion,wind)
      #self.speed.y=-0.3

      #lift,drag=self.do_it(self.rx,proportion)
      pass

    #self.speed.y+=lift*proportion
    #self.speed.z-=drag*proportion
    
    if self.y<-100. and soya.IDLER.state==1:
      self.got_out()

    self.add_mul_vector(proportion, self.speed)
    
    if self.looking==1:
      self.look_camera.rotate_lateral(proportion * self.rotation_speed)
      self.look_camera.add_mul_vector(proportion,self.speed)

    self.rotate_lateral(proportion * self.rotation_speed)

class Idler(idlers.SnowballzIdler):
  def start_waiting(self):
    global character
    global snow
    global camera

    self.game_rounds+=1

    # Loads the level, and put it in the scene
    self.state=1
    
    try: 
      scene.remove(level)
    except:
      pass
      #traceback.print_exc()

    print "Idler.start_level creating level and wind..."
    
    self.level = soya.World.get(self.levels[self.game_rounds])
    scene.add(self.level)

    wind=soya.Vector(scene,random.random()-.5,random.random()-.5,random.random()-.5)/2.

    try:
      level.remove(character)
      level.remove(character.particles)
      level.remove(snow)
    except:
      pass
      #traceback.print_exc()
    
    # Creates a character in the level, with a keyboard controler
    character = Character(self.level, KeyboardControler())
    character.set_xyz(*self.level.spawn_points[self.level.current_spawn])
    character.rotate_lateral(180.)
    character.power=100.
  
    snow=particles.Snow(scene)
    snow.move(character)

    # adds our fixed camera
    self.camera = soya.Camera(scene)
    self.camera.z=scene.get_box()[1].z+400
    self.camera.y=100.
    self.camera.back = 470.0
    self.camera.look_at(character)

    # for the waiting loop to move the camera around
    self.camera_move=-1.

    soya.set_root_widget(widget.Group())
    soya.root_widget.add(widget.FPSLabel())
    soya.root_widget.add(self.camera)

    self.make_text_widgets()

    self.make_title()
    
    self.move_widgets()

  def start_playing(self):
    print "start playin"
    soya.root_widget.remove(self.camera)

    soya.cursor_set_visible(0)
    soya.set_grab_input(1)

    pos=self.camera.position()

    self.camera = soya.TravelingCamera(scene)
    traveling=soya.ThirdPersonTraveling(character)
    traveling.distance=23
    traveling.top_view=0.6
    traveling.offset_y=4.
    self.camera.add_traveling(traveling)
              
    self.camera.move(pos)
    self.camera.back=470.
    self.camera.speed=0.7

    if SOUND:
      openal.init(self.camera)

    soya.set_root_widget(widget.Group())
    soya.root_widget.add(widget.FPSLabel())
    soya.root_widget.add(self.camera)

    self.make_overlay() 
    self.make_map_widget()
    self.make_compass_widget()
    self.make_text_widgets()
   
    self.move_widgets()

  def score(self,msg,bad=False,smallmsg=''):
    msgcol=(0.9,0.3,0.2,0.9)

    soya.root_widget.remove(self.score_label)
    soya.root_widget.remove(self.power_label)
    soya.root_widget.remove(self.altitude_label)
    soya.root_widget.remove(self.speed_label)
    
    message=soya.widget.Label(soya.root_widget,text="%s" % (msg),font=font.normal,color=msgcol)
    message.width=camera.width
    message.top=140
    message.align=1

    message=soya.widget.Label(soya.root_widget,text="bonus: %d" % character.bonus,font=font.small,color=msgcol)
    message.width=camera.width
    message.top=220
    message.align=1
    
    character.bonuscount+=1
    powerbonus=int(character.power) * character.bonuscount *5
    character.score+=powerbonus
    message=soya.widget.Label(soya.root_widget,text="power bonus: %d x %d x 5 = %d" % (int(character.power),character.bonuscount,powerbonus),font=font.small,color=msgcol)
    message.width=camera.width
    message.top=245
    message.align=1

    targetbonus=character.target_points*character.bonuscount
    character.score+=targetbonus
    message=soya.widget.Label(soya.root_widget,text="target: %d x %d = %d" % (character.target_points,character.bonuscount,targetbonus),font=font.small,color=msgcol)
    message.width=camera.width
    message.top=270
    message.align=1

    self.total_score+=character.score

    message=soya.widget.Label(soya.root_widget,text="score: %d" % (character.score),font=font.normal,color=msgcol)
    message.width=camera.width
    message.top=300
    message.align=1

    message=soya.widget.Label(soya.root_widget,text=smallmsg,font=font.small,color=msgcol)
    message.width=camera.width
    message.top=360
    message.align=1

    message=soya.widget.Label(soya.root_widget,text="total: %d" % self.total_score,font=font.normal,color=msgcol)
    message.width=camera.width
    message.top=400
    message.align=1

    try:
      character.play_animation(ANIM_STILL)
      
      smoke=soya.particle.FlagFirework(level)
      smoke.auto_generate_particle=True

      if not bad:
        smoke.set_colors((0.1, 0.1, 0.2, 0.4), (0.3, 0.3, 0.5, 0.4), (0.3, 0.3, 0.5, 0.4), (0.1, 0.1, 0.1, 0.4))
      else:
        smoke.set_colors((0.9, 0.1, 0.1, 0.4), (0.6, 0.3, 0.3, 0.4), (0.5, 0.3, 0.3, 0.4), (0.1, 0.1, 0.1, 0.4))

      smoke.scale(4.,2.,4.)
      self.fireworks=smoke

      self.fireworks.move(character)

      traveling = soya.ThirdPersonTraveling(character,smooth_move=1)
      #traveling=soya.FixTraveling(soya.Point(character,0.,6.,6),character,smooth_move=1)
      
      traveling.distance = 8.0

      camera.add_traveling(traveling)
      camera.back = 470.0

    except:
      import traceback
      traceback.print_exc()
      sys.exit()

    self.cmove=0.
    character.on_ground=1

    self.set_state(idlers.FINISHED)

  def advance_time_waiting(self,proportion):
    self.advance_time_default(proportion)
    self.camera.z+=self.camera_move*proportion
   
    b1,b2=scene.get_box()

    if self.camera.z>b2.z+500 or self.camera.z<b1.z+200:
      self.camera_move=-self.camera_move
  
    snow.move(self.camera)

    self.camera.look_at(character)
  
  def advance_time_finished(self,proportion):
    character.turn_lateral(9.*proportion)

    if self.cmove<50:
      character.y+=proportion*.4
      self.cmove+=proportion*.4

    self.fireworks.move(character)
  
    self.advance_time_default(proportion)  

  def begin_round_waiting(self):
    self.begin_round_default()

    for event in soya.process_event():
      print "brw",event
      if event[0] == sdlconst.KEYDOWN:
        print "kdown"
        if (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          self.stop() # Quit the game
        
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
        elif event[1]==sdlconst.K_SPACE:
          print "space"
          self.set_state(idlers.PLAYING) 

  def begin_round_playing(self):
    self.begin_round_default()
    self.mappointer.left=(character.x/10)-(self.mappointer.width/2)
    self.mappointer.top=(character.z/10)-(self.mappointer.height/2)

    c=character.front.copy()
    c.convert_to(scene)
    if c.x>0: 
      p=180.
    else:
      p=0.

    # FIXME: this isnt sane 
    self.compass.rotz=-(p+degrees(atan(c.z/c.x)))+90
  
  def begin_round_finished(self):
    self.fireworks.begin_round() 
    camera.begin_round()
    character.speed.x=character.speed.y=character.speed.z=0.
    character.play_animation(ANIM_JUMP)
    character.on_ground=1
        
    for event in soya.process_event():
      if event[0] == sdlconst.QUIT:
        sys.exit()
      elif event[0] == sdlconst.KEYDOWN:
        if   (event[1] == sdlconst.K_q) or (event[1] == sdlconst.K_ESCAPE):
          sys.exit() # Quit the game
        
        elif event[1] == sdlconst.K_F1:
          soya.render(); 
          soya.screenshot().save(os.path.join(os.path.dirname(sys.argv[0]), "results", os.path.basename(sys.argv[0])[:-3] + ".jpeg"))
        elif event[1]==sdlconst.K_SPACE:
          if self.game_rounds>=len(self.levels)-1:
            self.stop()
          else:
            self.set_state(idlers.WAITING)
 

  
def run(levelset=None,onelevel=None,commandline=False,spawn=0):
  global scene
  global level
  global snow
  global wind

  if not commandline:
    soya.IDLER.stop()

  # Create the scene (a world with no parent)
  scene = soya.World()

  level=None

  print "Starting Idler..."
  i=Idler(scene,levelset,onelevel)
  i.level.current_spawn=spawn
  character.set_xyz(*i.level.spawn_points[i.level.current_spawn])
  i.idle()  

  if not commandline:
    menu.MenuIdler().idle()

if __name__=='__main__':
  import optparse

  parser=optparse.OptionParser()
  parser.add_option("-l","--level",dest="level", help="level to load",metavar="LEVEL",default="snowballz.level1")
  parser.add_option("-s","--levelset",dest="levelset", help="levelset to load",metavar="LEVELSET",default=None)
  parser.add_option("-x","--spawn",dest="spawn", help="spawn to start at",metavar="SPAWN",default=0)
  parser.add_option("-o","--loop",dest="loop",action="store_true", help="loop level/levelset",default=False)

  options,args=parser.parse_args()

  if not options.level and not options.levelset:
    parser.print_help()
    sys.exit()
  
  soya.init(title="Snowball Surpise: Flying!", width=1024,height=768)

  try:
    options.spawn=int(options.spawn)
  except:
    traceback.print_exc()
    options.spawn=0

  print "---",options.spawn
  
  if options.loop:
    while 1:
      run(options.levelset,options.level,commandline=True,spawn=options.spawn)
  else:
    run(options.levelset,options.level,commandline=True,spawn=options.spawn)
    
 
