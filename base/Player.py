# Base class for a player
class Player:
    """
      Base class for a player
    """
    #public:

    def __init__(self):
        self.name = "Un-named" # players name 
        self.current_animation = "Stand_Still" # the animation currently being played 

    #public:

    def setName(self, name = "Un-named"): 
        """
        @name : name to set to

        set a players name

        """
        raise NotImplementedError()

    def playAnimation(self, animation): 
        """
        @animation : the animation to play

        changes the animation currently being played

        """
        raise NotImplementedError()


