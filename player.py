import soya
# from soya import ode

class PlayerVolume(soya.Cal3dVolume):
  def __init__(self, parent = None, shape = None, attached_meshes = None):
    soya.Cal3dVolume.__init__(self,parent,shape,attached_meshes)

    self.anim_speed=2.50

  def advance_time(self, proportion):
    soya.Cal3dVolume.advance_time(self,proportion*self.anim_speed)

class Player(soya.World):
  """A character in the game."""
  def __init__(self, parent, controler):
    print "Character init..."
    soya.World.__init__(self, parent)

    # Loads a Cal3D shape (=model)
    print "Character.init loading penguin..."
    penguin = soya.Cal3dShape.get("newpenguin")
  

    self.wrx=self.wry=self.wrz=self.rx=self.ry=self.rz=0.

    # Creates a Cal3D volume displaying the "penguin" shape
    # (NB penguin is the name of a player).
    print "Character.init creating volume..."
    self.perso = PlayerVolume(self, penguin)
    self.perso.rotate_vertical(-90.)
    self.perso.rotate_lateral(-180.)
    self.perso.scale(0.5,0.5,0.5)
  
    # Starts playing the idling animation in loop
    self.perso.animate_blend_cycle("Stand_Still")
   
    # The current animation
    self.current_animation = "Stand_Still"
    
    # Disable raypicking on the character itself !!!
    self.solid = 0
    
    self.controler      = controler
    self.speed          = soya.Vector(self)
    self.rotation_speed = 0.0
    
    # We need radius * sqrt(2)/2 < max speed (here, 0.35)
    self.radius         = 2.0
    self.radius_y       = 2.0
    self.center         = soya.Point(self, 0.0, self.radius_y, 0.0)
    
    self.left   = soya.Vector(self, -1.0,  0.0,  0.0)
    self.right  = soya.Vector(self,  1.0,  0.0,  0.0)
    self.down   = soya.Vector(self,  0.0, -1.0,  0.0)
    self.up     = soya.Vector(self,  0.0,  1.0,  0.0)
    self.front  = soya.Vector(self,  0.0,  0.0, -1.0)
    self.back   = soya.Vector(self,  0.0,  0.0,  1.0)

  def play_animation(self, animation):
    if self.current_animation != animation:
      # Stops previous animation
      self.perso.animate_clear_cycle(self.current_animation, 0.2)
      
      # Starts the new one
      self.perso.animate_blend_cycle(animation, 1.0, 0.2)
      
      self.current_animation = animation
     
  def begin_round(self):
    self.begin_action(self.controler.next())
    soya.World.begin_round(self)
    
  def begin_action(self, action):
    pass
  
  def advance_time(self, proportion):
    soya.World.advance_time(self, proportion)
