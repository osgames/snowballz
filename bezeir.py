#Coppyright 2004 by Matthew Marshall (mmarshall@myrealbox.com)
#GPL, yadda yadda yadda

from math import floor

class curve:
  def __init__(self,points):
    self.points = points
    if len(self.points) == 0 or len(self.points) > 4:
      raise ValueError(str(len(self.points)) + " is an invalide number of points for a curve!")
    self.point = getattr(self,"_point"+str(len(self.points)-1))  #Assign self.point to be the relevent function.
  def _point0(self,t):
    return self.points[0] #With only one point, there is only one possible output!
    
  def _point1(self,t): #First degree bezeir.  (This is really just a line segment, but could be useful.)
    p = self.points
    return p[0]*t+p[1]*(1-t)
    
  def _point2(self,t): #Second degree bezeir.  
    p = self.points
    return p[0]*t**2 + p[1]*2*t*(1-t) + p[2]*(1-t)**2
    
  def _point3(self,t): #Third degree bezeir.
    if t > 1.0: raise ValueError("t should not be above 1.0 (got %d)" % t)
    p = self.points
    return p[0]*t**3 + p[1]*3*t**2*(1-t) + p[2]*3*t*(1-t)**2 + p[3]*(1-t)**3
    
class circle:
  def __init__(self,points,degree=3):
    self.points = points
    if len(points) % degree: 
      raise ValueError("The number of points must be evenly divisible by the bezeir degree.  (Got %d points with degree %d.)" % (len(points),degree))
    
    self.curves = [None]*(len(points)/degree)
    for i in range(len(points)/degree):
      s = i*(degree)
      e = (i+1)*degree+1
      if e < len(points):
        self.curves[i] = curve(points[s:e])
      else: #This must be the last curve, in which case we mush add the first point to the end.
        self.curves[i] = curve(points[s:e] + [points[0]])
  
  def point(self,t):
    if t >= 1.0: return self.curves[-1].points[-1] #A value of 1 or greater will return an index error in the next line, so we will take care of it here.
    return self.curves[int(t*len(self.curves))].point(1.0-t*len(self.curves)+floor(t*len(self.curves)))
        
class curvelink(circle):
  def __init__(self,points,degree=3):
    self.points = points
    curvenum = (len(points)-1)/degree
    if (curvenum % 1): 
      raise ValueError("The number of points-1 must be evenly divisible by the bezeir degree.  (Got %d points with degree %d.)" % (len(points),degree))
    
    self.curves = [None]*curvenum
    for i in range(curvenum):
      self.curves[i] = curve(points[i*degree:(i+1)*degree+1])
